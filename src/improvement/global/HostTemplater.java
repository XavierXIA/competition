/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package improvement.global;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import model.app.Application;
import model.infra.Machine;
import model.infra.MachineType;

import common.Printer;
import common.Utils;

import control.Main;

public final class HostTemplater {
    private static Map<MachineType, HostTemplater> type2templater = new HashMap<>(2);
    // TODO : not generic
    public static final boolean isSeal = true;

    static final Set<Machine> templatedMachines = new HashSet<>();

    private final List<Template> template_list;
    private double threshold;
    private final double thresholdStep;
    private int occupiedNB;

    // TODO : recalculated
    private static final Comparator<Template> comparator = new Comparator<Template>() {
        @Override
        public int compare(Template t1, Template t2) {
            if (Utils.doubleEqual(t1.criterion, t2.criterion)) {
                int nb1 = t1.getTotalNB();
                int nb2 = t2.getTotalNB();

                return nb1 > nb2 ? -1 : nb1 == nb2 ? 0 : 1;
            }

            return t1.criterion < t2.criterion ? -1 : 1;
        }
    };

    private HostTemplater(double t, double s) {
        threshold = t;
        thresholdStep = s;
        template_list = new ArrayList<>();
    }

    public static void testAddTemplates4disk() {
        for (MachineType type : MachineType.type_list) {
            HostTemplater templater = type2templater.get(type);

            for (Machine m : Machine.considerMachines) {
                if (m.type == type && m.getAvDISK() < templater.threshold) {
                    Template t = new Template(m, m.getAvDISK());
                    if (!templater.template_list.contains(t)) {
                        templater.template_list.add(t);
                    }
                }
            }
        }
    }

    public static void testAddTemplates4cpu() {
        for (MachineType type : MachineType.type_list) {
            HostTemplater templater = type2templater.get(type);

            for (Machine m : Machine.considerMachines) {
                if (m.type == type) {
                    double dis = m.getDis2ExpectedCPU();
                    if (dis < templater.threshold) {
                        Template t = new Template(m, dis);
                        if (!templater.template_list.contains(t)) {
                            templater.template_list.add(t);
                        }
                    }
                }
            }
        }
    }

    public static boolean isTemplatable(Machine m) {
        // return false;
        return m.getDis2ExpectedCPU() < type2templater.get(m.type).threshold;
    }

    public static void incrThreshold() {
        for (HostTemplater templater : type2templater.values()) {
            templater.threshold += templater.thresholdStep;
        }
    }

    // return templated && not sealed machine nb
    public static int loadTemplates() {
        for (MachineType type : MachineType.type_list) {
            HostTemplater templater = type2templater.get(type);
            if (templater.template_list.isEmpty()) {
                continue;
            }

            templater.occupiedNB = 0;
            if (!isSeal) {
                for (Machine m : MachineSealer.machines) {
                    if (m.type == type) {
                        templater.occupiedNB += 1;
                    }
                }
            }

            Template.init();
            Collections.sort(templater.template_list, comparator);

            for (Template t : templater.template_list) {
                t.load(templater);
            }

            Template.finish();
        }

        if (!isSeal) {
            Machine.considerMachines.addAll(templatedMachines);
            templatedMachines.clear();
        }

        int count = 0;
        for (MachineType type : MachineType.type_list) {
            count += type2templater.get(type).occupiedNB;
            if (Main.debug) {
                Printer.print(type.capDISK + " : occupied machine nb : " + type2templater.get(type).occupiedNB);
            }
        }

        return count;
    }

    public static void addTemplater(MachineType type, double threshold, double step) {
        if (type2templater.containsKey(type)) {
            return;
        }

        type2templater.put(type, new HostTemplater(threshold, step));
    }

    public static void clear() {
        type2templater.clear();
    }

    public int getOccupiedNB() {
        return occupiedNB;
    }

    public void incrOccupiedNB() {
        occupiedNB++;
    }
}

class Template {
    private static int[] remainedInstNB;

    public final MachineType type;
    public final double criterion;
    private final Map<Application, Integer> template;

    Template(Machine m, double c) {
        type = m.type;
        template = new HashMap<>(m.getHostedApps());
        criterion = c;
    }

    void load(HostTemplater templater) {
        if (templater.getOccupiedNB() >= type.getExpectedNB() || isAllPlaced()) {
            return;
        }

        for (int i = Machine.considerMachines.size() - 1; i >= 0; i--) {
            Machine m = Machine.considerMachines.get(i);
            if (m.type == type && m.isEmpty()) {
                for (Entry<Application, Integer> entry : template.entrySet()) {
                    for (int j = 0; j < entry.getValue(); j++) {
                        m.add(entry.getKey());
                        entry.getKey().getInst(null).setTarHost(m);

                        remainedInstNB[entry.getKey().index]--;
                    }
                }

                if (Main.debug) {
                    m.debug();
                }

                Machine.considerMachines.remove(i);
                if (HostTemplater.isSeal) {
                    MachineSealer.addMachine(m);
                } else {
                    HostTemplater.templatedMachines.add(m);
                }

                templater.incrOccupiedNB();
                if (templater.getOccupiedNB() >= type.getExpectedNB() || isAllPlaced()) {
                    return;
                }
            }
        }
    }

    private boolean isAllPlaced() {
        for (Entry<Application, Integer> entry : template.entrySet()) {
            if (remainedInstNB[entry.getKey().index] < entry.getValue()) {
                return true;
            }
        }

        return false;
    }

    int getTotalNB() {
        int min = Integer.MAX_VALUE;
        for (Entry<Application, Integer> entry : template.entrySet()) {
            int count = entry.getKey().inst_list.size() / entry.getValue();
            if (min > count) {
                min = count;
            }
        }

        return min;
    }

    static void init() {
        remainedInstNB = new int[Application.app_list.size()];
        for (Application app : Application.app_list) {
            remainedInstNB[app.index] = app.getRemainedInstCount();
        }
    }

    static void finish() {
        remainedInstNB = null;
    }

    @Override
    public boolean equals(Object obj) {
        assert obj.getClass() == getClass() : "HostTemplate : input not instance of HostTemplate !";

        Map<Application, Integer> t = ((Template) obj).template;
        for (Entry<Application, Integer> entry : template.entrySet()) {
            if (entry.getValue() != t.get(entry.getKey())) {
                return false;
            }
        }

        return true;
    }
}
