/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package improvement.global;

import model.app.Application;
import model.infra.Machine;
import model.infra.MachineType;

import common.Utils;

// based on Genetic Algorithm
public final class ParaSetter {
    private static int expectedNB; // 4805 ?

    public static void setExpectedNB(int expectedNB) {
        ParaSetter.expectedNB = expectedNB;

        double[] sums = new double[Machine.timeSlotNB];
        for (int i = 0; i < sums.length; i++) {
            sums[i] = 0;
        }
        for (Application app : Application.app_list) {
            for (int i = 0; i < sums.length; i++) {
                sums[i] += app.reqCPU[i] * app.inst_list.size();
            }
        }

        for (MachineType type : MachineType.type_list) {
            if (type.capDISK == 1440) {
                double[] cpu1440 = { 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5,
                        0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5,
                        0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5,
                        0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5,
                        0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5,
                        0.5, 0.5, 0.5, 0.5, 0.5, 0.5 };
                type.setExpectedAvCpu(cpu1440);
                type.setCpuConCeil(0.51);
                type.setMeanCpuConCeil(0.5);
                type.setCpuDevCeilCoeff(1.7);
                type.setExpectedNB(expectedNB - 3000);

                HostTemplater.addTemplater(type, Utils.EPSILON, 0.1);
            } else {
                double lowCap = (expectedNB - 3000) * 0.5 * 32;
                double[] cpu2457 = new double[sums.length];

                double min = Double.MAX_VALUE;
                double sum = 0;
                for (int i = sums.length - 1; i >= 0; i--) {
                    cpu2457[i] = Math.min(1 - (sums[i] - lowCap) / 3000 / 92, 0.5);
                    sum += cpu2457[i];
                    if (min > cpu2457[i]) {
                        min = cpu2457[i];
                    }
                }

                type.setExpectedAvCpu(cpu2457);
                type.setMeanCpuConCeil(1 - min + 0.1);
                type.setMeanCpuConCeil((sums.length - sum) / sums.length);
                type.setCpuDevCeilCoeff(2.2);
                type.setExpectedNB(3000);

                HostTemplater.addTemplater(type, 30, 1);
            }
        }
    }

    public static int getExpectedNB() {
        return expectedNB;
    }
}

