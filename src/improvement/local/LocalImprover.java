/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package improvement.local;

import java.text.DecimalFormat;

import model.infra.Machine;

import common.Printer;
import common.Utils;

import control.Main;

public final class LocalImprover {
    private final double[] scores;

    public LocalImprover() {
        scores = new double[Machine.totalMachineNB];
    }

    // TODO : dyn threshold
    public void improve(int roundNB) {
        for (Machine m : Machine.getAllMachines()) {
            scores[m.index] = m.getScore();
        }

        Consolidator consolidator = new Consolidator(scores);

//        double coef = 1;
//        while (consolidator.consolidate(coef)) {
//            if (Main.debug) {
//                Printer.pass("machine nb : " + Machine.getOccupiedMachineNB());
//                Printer.print("consolidated score : " + getScore());
//            }
//            coef += 0.1;
//        }

        consolidator.consolidate(1);
        if (Main.debug) {
            Printer.pass("machine nb : " + Machine.getOccupiedMachineNB());
            Printer.print("consolidated score : " + getScore());
        }

        // Machine.analyse();

        CpuBalancer balancer = new CpuBalancer(scores);
//
        long startTime = Main.debug || Main.isPrint ? System.nanoTime() : 0;
//
//        for (int i = 0; i < roundNB; i++) {
            balancer.balance(1);
//        }
//
        if (Main.debug || Main.isPrint) {
            DecimalFormat formatter = new DecimalFormat("#.###");
            Printer.info("balancing time : " + formatter.format((System.nanoTime() - startTime) / 6e10));
        }
    }

    public double getScore() {
        double score = 0;
        for (int i = 0; i < scores.length; i++) {
            score += scores[i];
        }

        double check = 0;
        for (Machine m : Machine.getAllMachines()) {
            check += m.getScore();
        }
        assert Utils.doubleEqual(score, check) : "LocalImprover : score diff ! " + score + ", " + check;

        return score;
    }
}

