/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package improvement.local;

import improvement.global.ParaSetter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import model.app.Application;
import model.app.Instance;
import model.app.Interference;
import model.infra.Machine;

import common.Printer;

import control.Main;

//score oriented, can not guarantee -> a basic version first, or metaheuristics. full machines make sense
public final class Consolidator {
    private static double consolThreshold = 1;

    private final double[] scores;
    private final List<Application> unplacedApps;
    private List<Machine> tarMachine_list;

    private final Comparator<Application>[] appCompare;

    public Consolidator(double[] s) {
        scores = s;
        unplacedApps = new ArrayList<>();

        appCompare = new Comparator[2];
        appCompare[0] = new Comparator<Application>() {
            @Override
            public int compare(Application app1, Application app2) {
                if (app1.reqDISK == app2.reqDISK) {
                    int nb1 = Interference.getInterfNB(app1);
                    int nb2 = Interference.getInterfNB(app2);

                    return nb1 < nb2 ? -1 : nb1 == nb2 ? 0 : 1;
                }
                return app1.reqDISK < app2.reqDISK ? -1 : 1;
            }
        };
        appCompare[1] = new Comparator<Application>() {
            @Override
            public int compare(Application app1, Application app2) {
                return app1.reqDISK > app2.reqDISK ? -1 : app1.reqDISK == app2.reqDISK ? 0 : 1;
            }
        };
    }

    // return true if any machine reconfigured
    public boolean consolidate(double speedupCoef) {
        assert speedupCoef >= 1 : "LoadBalancer2 : invalid speedupCoef !";

        if (Machine.getOccupiedMachineNB() <= ParaSetter.getExpectedNB()) {
            return false;
        }

        boolean isUpdated = consolidate4score(speedupCoef);

        // while (consolidate4disk()) {
        // isUpdated |= true;
        // }
        // for (Machine m : Machine.considerMachines) {
        // scores[m.index] = m.getScore();
        // }

        return isUpdated;
    }

    private boolean consolidate4score(double speedupCoef) {
        Machine.updateCriteria(Machine.CriterMode.CPU_LOAD);
        Collections.sort(Machine.considerMachines, Machine.ascendCompare);

        int emptyNB = Machine.totalMachineNB - Machine.getOccupiedMachineNB();
        boolean isUpdated = false;
        double sum = 0;

        int consideredMachineNB = Machine.considerMachines.size();
        int nb = (int) (consideredMachineNB / speedupCoef);
        Machine src, tar, bestTar;
        double cost, minCost;
        Application app;
        for (int i = emptyNB; i < nb; i++) {
            src = Machine.considerMachines.get(i);

            // TODO : not generic
            if (src.type.capDISK != 1440) {
                continue;
            }
            assert !src.isEmpty() : "Consolidator : unexpected machine state !";
            ChangeRecorder.clear();
            sum = 0;
            boolean isFail = false;
            for (Entry<Application, Integer> entry : new HashMap<Application, Integer>(src.getHostedApps()).entrySet()) {
                app = entry.getKey();
                for (int j = 0; j < entry.getValue(); j++) {
                    minCost = Double.MAX_VALUE;
                    bestTar = null;

                    for (int k = emptyNB; k < consideredMachineNB; k++) {
                        tar = Machine.considerMachines.get(k);
                        if (!tar.isEmpty() && tar != src) {
                            if (tar.isConformHardConstr(app)) {
                                cost = tar.testAddScore(app) - scores[tar.index];
                                if (minCost > cost) {
                                    minCost = cost;
                                    bestTar = tar;
                                }
                            }
                        }
                    }

                    if (bestTar != null && minCost < consolThreshold) {
                        ChangeRecorder.perform(src, bestTar, app, app.getInst(src));
                        sum += minCost;
                        scores[bestTar.index] += minCost;
                    } else {
                        isFail = true;
                        j = entry.getValue();
                    }
                }

                if (isFail) {
                    break;
                }
            }

            if (isFail || sum > consolThreshold) {
                Set<Machine> changedMachines = ChangeRecorder.rollback();
                for (Machine m : changedMachines) {
                    scores[m.index] = m.getScore();
                }
            } else {
                scores[src.index] = 0;
                Machine.decrOccupiedMachineNB();
                isUpdated = true;

                if (Machine.getOccupiedMachineNB() <= ParaSetter.getExpectedNB()) {
                    break;
                }
            }
        }

        if (Main.debug) {
            assert Machine.getHostedInstCount() == Application.getInstCount() : "Consolidator : Hosted inst count does not equal to total inst count!";
            assert Application.getHostedInstCount() == Application.getInstCount() : "Consolidator : Hosted inst count does not equal to total inst count!";
            for (Machine machine : Machine.getAllMachines()) {
                machine.debug();
            }
        }

        return isUpdated;
    }

    // TODO : not generic
    // TODO : reverse tarMachine_list ?
    private boolean consolidate4disk() {
        boolean isUpdated = false;
        Collections.sort(Machine.considerMachines, new Comparator<Machine>() {
            @Override
            public int compare(Machine m1, Machine m2) {
                double load1 = m1.type.capDISK - m1.getAvDISK();
                double load2 = m2.type.capDISK - m2.getAvDISK();

                return load1 > load2 ? 1 : load1 == load2 ? 0 : -1;
            }
        });

        int emptyNB = Machine.totalMachineNB - Machine.getOccupiedMachineNB();
        int consideredMachineNB = Machine.considerMachines.size();
        tarMachine_list = new ArrayList<>(Machine.considerMachines.subList(emptyNB, consideredMachineNB));
        Collections.sort(tarMachine_list, new Comparator<Machine>() {
            @Override
            public int compare(Machine m1, Machine m2) {
                return m1.getAvDISK() < m2.getAvDISK() ? -1 : m1.getAvDISK() == m2.getAvDISK() ? 0 : 1;
            }
        });

        for (int i = emptyNB; i < consideredMachineNB; i++) {
            Machine m = Machine.considerMachines.get(i);

            // TODO : not generic
            if (m.type.capDISK != 600) {
                continue;
            }
            assert !m.isEmpty() : "Consolidator : unexpected machine state !";
            tarMachine_list.remove(m);

            if (!empty(m)) {
                Machine.decrOccupiedMachineNB();
                isUpdated = true;
            } else {
                tarMachine_list.add(m);
            }
        }

        if (!unplacedApps.isEmpty()) {
            for (Machine machine : tarMachine_list) {
                treatUnplacedApps(machine, null);
            }
        }
        while (!unplacedApps.isEmpty()) {
            treatUnplacedApps(getEmptyMachine(), null);
            Machine.incrOccupiedMachineNB();
        }

        if (Main.debug) {
            Printer.print("machine nb : " + Machine.getOccupiedMachineNB());

            assert Machine.getHostedInstCount() == Application.getInstCount() : "Consolidator : Hosted inst count does not equal to total inst count!";
            assert Application.getHostedInstCount() == Application.getInstCount() : "Consolidator : Hosted inst count does not equal to total inst count!";
            for (Machine machine : Machine.getAllMachines()) {
                machine.debug();
            }
        }

        return isUpdated;
    }

    // return true if failed
    private boolean empty(Machine src) {
        Application nextApp;
        while (!src.isEmpty()) {
            int max = 0;
            nextApp = null;
            for (Application app : src.getHostedApps().keySet()) {
                if (app.reqDISK > max) {
                    max = app.reqDISK;
                    nextApp = app;
                }
            }

            assert nextApp != null : "Consolidator : no app found !";
            if (offload(nextApp, src)) {
                return true;
            }
        }

        return false;
    }

    // return true if fail
    private boolean offload(Application app, Machine src) {
        Application nextApp = app;
        Instance nextInst = app.getInst(src);
        src.remove(app);
        nextInst.setTarHost(null);

        int tarMachineNB = tarMachine_list.size();
        Machine tar;
        Application exApp;
        for (int i = 0; i < tarMachineNB; i++) {
            int tarIdx = i;
            tar = tarMachine_list.get(i);
            if (treatUnplacedApps(tar, nextInst)) {
                tarMachine_list.remove(i); // remove tar
                tarIdx = getIndexWrtDisk(tar.getAvDISK(), i);
                tarMachine_list.add(tarIdx, tar);
            }

            if (tar.type.capDISK - tar.getAvDISK() < src.type.capDISK - src.getAvDISK()) {
                continue;
            }

            if (tar.isConformHardConstr(nextApp)) {
                tar.add(nextApp);
                nextInst.setTarHost(tar);

                tarMachine_list.remove(tarIdx);
                tarMachine_list.add(getIndexWrtDisk(tar.getAvDISK(), i), tar);

                return false;
            } else {
                exApp = exchange(nextApp, nextInst, tar);
                if (exApp != null) {
                    nextInst = exApp.getInst(tar);
                    nextInst.setTarHost(null);
                    nextApp = exApp;

                    tarMachine_list.remove(tarIdx);
                    tarMachine_list.add(getIndexWrtDisk(tar.getAvDISK(), i), tar);

                    i = -1;
                }
            }
        }

        if (src.isConformHardConstr(nextApp)) {
            src.add(nextApp);
            nextInst.setTarHost(src);
        } else {
            unplacedApps.add(nextApp);
            Collections.sort(unplacedApps, appCompare[1]);
        }

        return true;
    }

    // return null if fail
    private Application exchange(Application srcApp, Instance srcInst, Machine tar) {
        List<Application> app_list = new ArrayList<>(tar.getHostedApps().keySet());
        Collections.sort(app_list, appCompare[0]);

        for (Application tarApp : app_list) {
            if (tarApp.reqDISK > srcApp.reqDISK) {
                return null;
            }

            if (tarApp.reqDISK < srcApp.reqDISK) {
                tar.remove(tarApp);
                if (tar.isConformHardConstr(srcApp)) {
                    tar.add(srcApp);
                    srcInst.setTarHost(tar);

                    return tarApp;
                } else {
                    tar.add(tarApp);
                }
            }
        }

        return null;
    }

    private boolean treatUnplacedApps(Machine m, Instance selectedInst) {
        boolean isUpdated = false;

        Iterator<Application> iterator = unplacedApps.iterator();
        Application app;
        while (iterator.hasNext()) {
            app = iterator.next();

            if (m.isConformHardConstr(app)) {
                m.add(app);

                for (Instance inst : app.inst_list) {
                    if (!inst.isPlaced() && inst != selectedInst) {
                        inst.setTarHost(m);
                        break;
                    }
                }
                iterator.remove();
                isUpdated = true;
            }
        }

        return isUpdated;
    }

    private Machine getEmptyMachine() {
        for (Machine m : Machine.considerMachines) {
            if (m.isEmpty()) {
                return m;
            }
        }

        return null;
    }

    private int getIndexWrtDisk(int disk, int upper) {
        int start = 0;
        if (disk <= tarMachine_list.get(start).getAvDISK()) {
            return 0;
        }

        int end = upper - 1;
        if (disk > tarMachine_list.get(end).getAvDISK()) {
            return upper;
        }

        int index;
        while (end > start + 1) {
            index = Math.max((start + end) >> 1, start + 1);

            if (disk > tarMachine_list.get(index).getAvDISK()) {
                start = index;
            } else {
                end = index;
            }
        }

        return end;
    }
}

