/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package improvement.local;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import model.app.Application;
import model.app.Instance;
import model.infra.Machine;

final class ChangeRecorder {
    public static final List<ChangeRecorder> changes = new ArrayList<>();

    public final Machine src;
    public final Machine tar;
    public final Application app;
    public final Instance inst;

    public ChangeRecorder(Machine s, Machine t, Application a, Instance i) {
        src = s;
        tar = t;
        app = a;
        inst = i;
    }

    public static void perform(Machine src, Machine tar, Application app, Instance inst) {
        src.remove(app);
        tar.add(app);
        inst.setTarHost(tar);

        changes.add(new ChangeRecorder(src, tar, app, inst));
    }

    public static Set<Machine> rollback() {
        Set<Machine> changedMachines = new HashSet<>();
        for (ChangeRecorder r : changes) {
            r.tar.remove(r.app);
            r.src.add(r.app);
            r.inst.setTarHost(r.src);

            changedMachines.add(r.src);
            changedMachines.add(r.tar);
        }

        return changedMachines;
    }

    public static void clear() {
        changes.clear();
    }
}

