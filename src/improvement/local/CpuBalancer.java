/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package improvement.local;

import improvement.global.HostTemplater;
import improvement.global.MachineSealer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import model.app.Application;
import model.app.Instance;
import model.infra.Machine;

import common.Timer;
import common.Utils;

import control.Main;

public final class CpuBalancer {
    // TODO : not genric
    private static final double templateScoreThreshold = 0.02;
    private static int timeout = -1;

    private double[] scores;
    private boolean[] offloadFail;
    private double[] scoreDists;
    private static double[] appCpuDists;

    private final List<Machine> overloaded;
    private final List<Machine> underloaded;

    private double speedupCoef;
    private boolean isStrongCond = true;

    public enum Func {
        First, BestScore, BestDis
    };

    private static Func func = Func.First;

    private static final OffloadComparator offSrcAppCompare = new OffloadComparator();
    private static final Comparator<Application> exSrcAppCompare = new Comparator<Application>() {
        @Override
        public int compare(Application app1, Application app2) {
            if (app1.meanCPU == app2.meanCPU) {
                return app1.maxCPU > app2.maxCPU ? -1 : app1.maxCPU == app2.maxCPU ? 0 : 1;
            }
            return app1.meanCPU > app2.meanCPU ? -1 : 1;
        }
    };
    private static final Comparator<Application> exTarAppCompare = new Comparator<Application>() {
        @Override
        public int compare(Application app1, Application app2) {
            double d1 = appCpuDists[app1.index];
            double d2 = appCpuDists[app2.index];

            return d1 > d2 ? 1 : d1 == d2 ? 0 : -1;
        }
    };

    // TODO
    // private static final ExchangeTarComparator reExTarAppCompare = new
    // ExchangeTarComparator();

    public CpuBalancer(double[] s) {
        scores = s;
        offloadFail = new boolean[scores.length];
        scoreDists = new double[scores.length];
        appCpuDists = new double[Application.app_list.size()];

        int nb = Machine.getOccupiedMachineNB() - MachineSealer.machines.size();
        overloaded = new ArrayList<>(nb);
        underloaded = new ArrayList<>(nb / 2);

        for (Machine m : Machine.considerMachines) {
            if (!m.isEmpty()) {
                scoreDists[m.index] = scores[m.index] - m.type.getExpectedScore();

                if (scoreDists[m.index] > 0) {
                    overloaded.add(m);
                } else {
                    underloaded.add(m);
                }
            }
        }

        Collections.sort(overloaded, new Comparator<Machine>() {
            @Override
            public int compare(Machine m1, Machine m2) {
                double d1 = scoreDists[m1.index];
                double d2 = scoreDists[m2.index];

                return d1 > d2 ? 1 : d1 == d2 ? 0 : -1;
            }
        });
    }

    public void setStrongCond(boolean isStrongCond) {
        this.isStrongCond = isStrongCond;
    }

    // return true if any machine reconfigured
    public boolean balance(double c) {
        assert c >= 1 : "LoadBalancer2 : invalid speedupCoef !";

        speedupCoef = c;
        boolean isUpdated = false;

        Timer timer = timeout > 0 ? Timer.startNewTimer(timeout) : null;
        // TODO : speedupCoef
        while (!overloaded.isEmpty()) {
            Machine src = overloaded.get(overloaded.size() - 1);
            boolean isFail = true;

            if (!offloadFail[src.index]) {// offload
                isFail = offload(src, underloaded);

                if (isFail) {
                    isFail = offload(src, overloaded);
                }

                if (isFail) {
                    offloadFail[src.index] = true;
                }
            }

            if (isFail) {// exchange
                switch (func) {
                    case First:
                        isFail = exchangeFirst(src);
                        break;
                    case BestScore:
                        isFail = exchangeBestScore(src);
                        break;
                    case BestDis:
                        isFail = exchangeBestDis(src);
                        break;
                }
            }

            if (!isFail) {
                isUpdated = true;

                boolean b = overloaded.remove(src);
                assert b : "LoadBalancer : the list does not contain the machine to rm !";

                scoreDists[src.index] = scores[src.index] - src.type.getExpectedScore();
                if (Math.abs(scoreDists[src.index]) > templateScoreThreshold || !HostTemplater.isTemplatable(src)) {
                    if (scoreDists[src.index] > 0) {
                        overloaded.add(getIndexWrtDis(scoreDists[src.index]), src);
                    } else {
                        underloaded.add(src);
                    }
                }
            } else {
                int i = overloaded.size() - 1;
                if (overloaded.get(i) == src) {
                    overloaded.remove(i);
                } else {
                    boolean b = overloaded.remove(src);
                    assert b : "LoadBalancer : the list does not contain the machine to rm !";
                }
            }

            if (timeout > 0 && timer.isTimeOut()) {
                break;
//                double sum = 0;
//                for (double s : scores) {
//                    sum += s;
//                }
//                Printer.print("score : " + sum);

//                PlaceIO_Manager.save(sum + ".csv");
//                timer = Timer.startNewTimer(timeout);
            }
        }

        appCpuDists = null;

        if (Main.debug) {
            assert Machine.getHostedInstCount() == Application.getInstCount() : "LocalImprover : Hosted inst count does not equal to total inst count!";
            assert Application.getHostedInstCount() == Application.getInstCount() : "Consolidator : Hosted inst count does not equal to total inst count!";
            for (Machine machine : Machine.getAllMachines()) {
                machine.debug();
            }
        }

        return isUpdated;
    }

    // return true if fail
    private boolean offload(Machine src, List<Machine> tarList) {
        List<Application> apps = new ArrayList<>(src.getHostedApps().keySet());
        offSrcAppCompare.setMachine(src);
        Collections.sort(apps, offSrcAppCompare);

        int j = (int) (tarList == overloaded ? overloaded.size() / speedupCoef - 1 : underloaded.size());
        for (Application app : apps) {
            for (int i = 0; i < j; i++) {
                Machine tar = tarList.get(i);
                assert src != tar : "unexpected state";

                boolean isFail = offload(app, src, tar);

                if (!isFail) {
                    assert tarList.get(i) == tar : "LoadBalancer : underloaded.get(j) != tar";
                    tarList.remove(i);

                    scoreDists[tar.index] = scores[tar.index] - tar.type.getExpectedScore();
                    if (tarList == overloaded || Math.abs(scoreDists[tar.index]) > templateScoreThreshold
                            || !HostTemplater.isTemplatable(tar)) {
                        if (scoreDists[tar.index] > 0) {
                            overloaded.add(getIndexWrtDis(scoreDists[tar.index]), tar);
                        } else {
                            underloaded.add(tar);
                        }
                    }

                    return false;
                }
            }
        }

        return true;
    }

    // return true if fail
    private boolean offload(Application app, Machine src, Machine tar) {
        if (!tar.isConformHardConstr(app)) {
            return true;
        }

        double rmScore = src.testRmScore(app);
        double addScore = tar.testAddScore(app);

        if (scores[src.index] + scores[tar.index] - rmScore - addScore > Utils.EPSILON) {
            src.remove(app);
            tar.add(app);
            app.getInst(src).setTarHost(tar);

            scores[src.index] = rmScore;
            scores[tar.index] = addScore;

            return false;
        } else {
            return true;
        }
    }

    // return true if fail
    private boolean exchangeFirst(Machine src) {
        List<Application> apps = new ArrayList<>(src.getHostedApps().keySet());
        Collections.sort(apps, exSrcAppCompare);

        for (Application srcApp : apps) {
            src.remove(srcApp);

            // TODO : check if constraint is necessary
            for (Application tarApp : Application.app_list) {
                appCpuDists[tarApp.index] = src.testAddDis2ExpectedCPU(tarApp);
            }
            Collections.sort(Application.app_list, exTarAppCompare);

            for (Application tarApp : Application.app_list) {
                if (tarApp == srcApp) {
                    if (isStrongCond) {
                        break;
                    } else {
                        continue;
                    }
                }

                for (Instance inst : tarApp.inst_list) {
                    Machine tar = inst.getTarHost();
                    if (isStrongCond && MachineSealer.isSealed(tar)) {
                        continue;
                    }

                    if (tar != src) {
                        tar.remove(tarApp);
                        double srcScore = src.testAddScore(tarApp);
                        double tarScore = tar.testAddScore(srcApp);

                        // TODO : check score first / constraint first
                        if (scores[src.index] + scores[tar.index] - srcScore - tarScore > Utils.EPSILON
                                && src.isConformHardConstr(tarApp) && tar.isConformHardConstr(srcApp)) {
                            src.add(tarApp);
                            tar.add(srcApp);

                            srcApp.getInst(src).setTarHost(tar);
                            tarApp.getInst(tar).setTarHost(src);

                            scores[src.index] = srcScore;
                            scores[tar.index] = tarScore;

                            if (!underloaded.remove(tar)) {
                                overloaded.remove(tar);
                            }

                            scoreDists[tar.index] = scores[tar.index] - tar.type.getExpectedScore();
                            if (Math.abs(scoreDists[tar.index]) > templateScoreThreshold
                                    || !HostTemplater.isTemplatable(tar)) {
                                if (scoreDists[tar.index] > 0) {
                                    overloaded.add(getIndexWrtDis(scoreDists[tar.index]), tar);
                                } else {
                                    underloaded.add(tar);
                                }
                            }

                            return false;
                        }
                        tar.add(tarApp);
                    }
                }
            }

            src.add(srcApp);
        }

        return true;
    }

    // return true if fail
    private boolean exchangeBestDis(Machine src) {
        List<Application> apps = new ArrayList<>(src.getHostedApps().keySet());
        Collections.sort(apps, exSrcAppCompare);
        Map<Machine, Double> machine2disVar = new HashMap<>();

        for (Application srcApp : apps) {
            src.remove(srcApp);

            // TODO : check if constraint is necessary
            for (Application tarApp : Application.app_list) {
                appCpuDists[tarApp.index] = src.testAddDis2ExpectedCPU(tarApp);
            }
            Collections.sort(Application.app_list, exTarAppCompare);

            for (Application tarApp : Application.app_list) {
                if (tarApp == srcApp) {
                    if (isStrongCond) {
                        break;
                    } else {
                        continue;
                    }
                }

                boolean isFail = true;
                Machine tar = null;
                for (Instance inst : tarApp.inst_list) {
                    tar = inst.getTarHost();
                    if (isStrongCond && MachineSealer.isSealed(tar)) {
                        continue;
                    }

                    if (tar != src) {
                        tar.remove(tarApp);
                        // TODO : check score first / constraint first
                        if (scores[src.index] + scores[tar.index] - src.testAddScore(tarApp) - tar.testAddScore(srcApp) > Utils.EPSILON
                                && src.isConformHardConstr(tarApp) && tar.isConformHardConstr(srcApp)) {
                            machine2disVar.put(tar,
                                    tar.testAddDis2ExpectedCPU(srcApp) - tar.testAddDis2ExpectedCPU(tarApp));
                            isFail = false;
                        }
                        tar.add(tarApp);
                    }
                }

                if (!isFail) {
                    double min = Double.MAX_VALUE;
                    for (Entry<Machine, Double> entry : machine2disVar.entrySet()) {
                        if (min > entry.getValue()) {
                            min = entry.getValue();
                            tar = entry.getKey();
                        }
                    }

                    tar.remove(tarApp);

                    scores[src.index] = src.testAddScore(tarApp);
                    scores[tar.index] = tar.testAddScore(srcApp);

                    src.add(tarApp);
                    tar.add(srcApp);

                    srcApp.getInst(src).setTarHost(tar);
                    tarApp.getInst(tar).setTarHost(src);

                    if (!underloaded.remove(tar)) {
                        overloaded.remove(tar);
                    }

                    scoreDists[tar.index] = scores[tar.index] - tar.type.getExpectedScore();
                    if (Math.abs(scoreDists[tar.index]) > templateScoreThreshold || !HostTemplater.isTemplatable(tar)) {
                        if (scoreDists[tar.index] > 0) {
                            overloaded.add(getIndexWrtDis(scoreDists[tar.index]), tar);
                        } else {
                            underloaded.add(tar);
                        }
                    }

                    return false;
                }
            }

            src.add(srcApp);
        }

        return true;
    }

    // return true if fail
    private boolean exchangeBestScore(Machine src) {
        List<Application> apps = new ArrayList<>(src.getHostedApps().keySet());
        Collections.sort(apps, exSrcAppCompare);
        Map<Machine, Double> machine2score = new HashMap<>();

        for (Application srcApp : apps) {
            src.remove(srcApp);

            // TODO : check if constraint is necessary
            for (Application tarApp : Application.app_list) {
                appCpuDists[tarApp.index] = src.testAddDis2ExpectedCPU(tarApp);
            }
            Collections.sort(Application.app_list, exTarAppCompare);

            for (Application tarApp : Application.app_list) {
                if (tarApp == srcApp) {
                    if (isStrongCond) {
                        break;
                    } else {
                        continue;
                    }
                }

                boolean isFail = true;
                Machine tar = null;
                for (Instance inst : tarApp.inst_list) {
                    tar = inst.getTarHost();
                    if (isStrongCond && MachineSealer.isSealed(tar)) {
                        continue;
                    }

                    if (tar != src) {
                        tar.remove(tarApp);
                        double sum = src.testAddScore(tarApp) + tar.testAddScore(srcApp);
                        // TODO : check score first / constraint first
                        if (scores[src.index] + scores[tar.index] - sum > Utils.EPSILON
                                && src.isConformHardConstr(tarApp) && tar.isConformHardConstr(srcApp)) {
                            // TODO : check use dis var or score
                            machine2score.put(tar, sum);
                            isFail = false;
                        }
                        tar.add(tarApp);
                    }
                }

                if (!isFail) {
                    double min = Double.MAX_VALUE;
                    for (Entry<Machine, Double> entry : machine2score.entrySet()) {
                        if (min > entry.getValue()) {
                            min = entry.getValue();
                            tar = entry.getKey();
                        }
                    }

                    tar.remove(tarApp);

                    scores[src.index] = src.testAddScore(tarApp);
                    scores[tar.index] = tar.testAddScore(srcApp);

                    src.add(tarApp);
                    tar.add(srcApp);

                    srcApp.getInst(src).setTarHost(tar);
                    tarApp.getInst(tar).setTarHost(src);

                    if (!underloaded.remove(tar)) {
                        overloaded.remove(tar);
                    }

                    scoreDists[tar.index] = scores[tar.index] - tar.type.getExpectedScore();
                    if (Math.abs(scoreDists[tar.index]) > templateScoreThreshold || !HostTemplater.isTemplatable(tar)) {
                        if (scoreDists[tar.index] > 0) {
                            overloaded.add(getIndexWrtDis(scoreDists[tar.index]), tar);
                        } else {
                            underloaded.add(tar);
                        }
                    }

                    return false;
                }
            }

            src.add(srcApp);
        }

        return true;
    }

    private int getIndexWrtDis(double d) {
        if (overloaded.isEmpty()) {
            return 0;
        }

        int end = overloaded.size() - 1;
        if (d >= scoreDists[overloaded.get(end).index]) {
            return overloaded.size();
        }

        int start = 0;
        if (scoreDists[overloaded.get(start).index] >= d) {
            return start;
        }

        int index;
        while (end > start + 1) {
            index = Math.max((start + end) >> 1, start + 1);

            if (d >= scoreDists[overloaded.get(index).index]) {
                start = index;
            } else {
                end = index;
            }
        }

        return end;
    }

    public static void setFunc(Func f) {
        func = f;
    }

    public static void setTimeout(int timeout) {
        CpuBalancer.timeout = timeout;
    }
}

class OffloadComparator implements Comparator<Application> {
    Machine m = null;

    void setMachine(Machine m) {
        this.m = m;
    }

    @Override
    public int compare(Application app1, Application app2) {
        double d1 = m.testRmDis2ExpectedCPU(app1);
        double d2 = m.testRmDis2ExpectedCPU(app2);

        return d1 < d2 ? -1 : d1 == d2 ? 0 : 1;
    }
};

