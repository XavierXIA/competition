/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package common;

public final class Timer implements Runnable {
    private final int timeout;
    private volatile boolean isTimeout = false;

    // unit : second
    private Timer(int t) {
        timeout = t * 60000;
        isTimeout = false;
    }

    public static Timer startNewTimer(int t) {
        Timer timer = new Timer(t);
        Thread thread = new Thread(timer);
        thread.setDaemon(true);
        thread.start();

        return timer;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(timeout);
            isTimeout = true;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean isTimeOut() {
        return isTimeout;
    }
}

