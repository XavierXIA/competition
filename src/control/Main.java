/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package control;

import improvement.global.HostTemplater;
import improvement.global.MachineSealer;
import improvement.global.ParaSetter;
import improvement.local.CpuBalancer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;

import model.app.AppParser;
import model.app.Application;
import model.app.Instance;
import model.infra.InfraParser;
import model.infra.Machine;
import model.infra.MachineType;
import placement.Placer;
import scheduling.Scheduler;

import common.Printer;

public class Main {
    public static final boolean debug = false;
    public static final boolean isPrint = true;

    public static void main(String args[]) {
        boolean isPlace = true;
        boolean isSchedule = true;
        boolean isExtreme = false;
        boolean isStrongCond = true;
        int roundNB = 1;

        // TODO : not generic
        String placement = "";
        String templateThresholds = "";
        int expectedNB = 4815;

        String infraPath = "";
        String appPath = "";
        String interfPath = "";
        String instancePath = "";

        // parse parameters
        for (int i = 0; i < args.length; i++) {
            switch (args[i]) {
                case "-infra":
                    i++;
                    infraPath = args[i];
                    break;

                case "-app":
                    i++;
                    appPath = args[i];
                    break;

                case "-interf":
                    i++;
                    interfPath = args[i];
                    break;

                case "-inst":
                    i++;
                    instancePath = args[i];
                    break;

                case "-improveTimeout":
                    i++;
                    CpuBalancer.setTimeout(Integer.parseInt(args[i]));
                    break;

                case "-placement":
                    i++;
                    placement = args[i];
                    break;

                case "-extreme":
                    isExtreme = true;
                    break;

                case "-WeakCond":
                    isStrongCond = false;
                    break;

                case "-BestScore":
                    CpuBalancer.setFunc(CpuBalancer.Func.BestScore);
                    break;
                case "-BestDis":
                    CpuBalancer.setFunc(CpuBalancer.Func.BestDis);
                    break;

                case "-expectedNB":
                    i++;
                    expectedNB = Integer.parseInt(args[i]);
                    break;

                case "-schedule":
                    isSchedule = true;
                    break;

                case "-roundNB":
                    i++;
                    roundNB = Integer.parseInt(args[i]);
                    break;

                case "-cpuStableThreshold":
                    i++;
                    Application.setCpuStableThreshold(Double.parseDouble(args[i]));
                    break;

                case "-templateThresholds":
                    i++;
                    templateThresholds = args[i];
                    break;

                case "-color":
                    Printer.enableColor();
                    break;

                default:
                    Printer.err("undefined option: " + args[i]);
            }
        }

        try {
            InfraParser.parse(infraPath);
            AppParser.parseApp(appPath);
            AppParser.parseInstance(instancePath);
            AppParser.parseInterference(interfPath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        ParaSetter.setExpectedNB(expectedNB);
        if (!templateThresholds.isEmpty()) {
            int count = 0;
            for (String threshold : templateThresholds.split(":")) {
                String[] s = threshold.split(",");
                HostTemplater.addTemplater(MachineType.type_list.get(count), Double.parseDouble(s[0]),
                        Double.parseDouble(s[1]));
            }
        }

        if (!placement.isEmpty()) {
            if (placement.indexOf(",") >= 0) {
                for (String s : placement.split(",")) {
                    if (!s.isEmpty()) {
                        PlaceIO_Manager.load(s);
                        HostTemplater.testAddTemplates4cpu();

                        if (debug) {
                            Printer.print(s + " studied");
                            for (Machine m : Machine.considerMachines) {
                                if (!m.isEmpty()) {
                                    Printer.print(m.type.capCPU + " : " + m.getDis2ExpectedCPU());
                                }
                            }
                        }

                        Machine.resetAll();
                        Instance.resetAll();
                    }
                }
            } else {
                PlaceIO_Manager.load(placement);
                if (debug || isPrint) {
                    Printer.print(placement + " loaded");
                }

                if (isExtreme) {
//                    MachineType.type_list.get(1).setExpectedScore(0);
                    for (MachineType type : MachineType.type_list) {
                        type.setExpectedScore(1);
                    }
                }

                int count = 0;
                double[] scores = new double[Machine.totalMachineNB];
                for (Machine m : Machine.getAllMachines()) {
                    scores[m.index] = m.getScore();
                    if (!m.isEmpty()) {
                        count++;
                    }
                }
                Machine.setOccupiedMachineNB(count);

                for (int i = 0; i < roundNB; i++) {
                    MachineSealer.sealMachines();

                    CpuBalancer balancer = new CpuBalancer(scores);
                    balancer.setStrongCond(isStrongCond);
                    balancer.balance(1);

                    if (isPrint) {
                        double score = 0;
                        for (double s : scores) {
                            score += s;
                        }

                        Printer.print(i + ", score : " + score);
                        PlaceIO_Manager.save(score + ".csv");
                    }

                    HostTemplater.incrThreshold();
                }

                isPlace = false;
            }
        }

        Application.clearIdAppMap();

        if (isPlace) {
            long startTime = debug || isPrint ? System.nanoTime() : 0;

            new Placer().place(roundNB);

            if (debug || isPrint) {
                DecimalFormat formatter = new DecimalFormat("#.###");
                Printer.info("placement time : " + formatter.format((System.nanoTime() - startTime) / 6e10));
            }
        }

        if (isSchedule) {
            long startTime = debug || isPrint ? System.nanoTime() : 0;

            try {
                new Scheduler().getSchedule().save();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (debug || isPrint) {
                DecimalFormat formatter = new DecimalFormat("#.###");
                Printer.info("scheduling time : " + formatter.format((System.nanoTime() - startTime) / 6e10));
            }
        }
    }
}

