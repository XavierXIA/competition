/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package control;

import improvement.global.ParaSetter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

import model.app.Application;
import model.app.Instance;
import model.infra.Machine;

public class PlaceIO_Manager {
    public static void save(String fileName) {
        File file = new File("submit/" + ParaSetter.getExpectedNB() + "/" + fileName);
        if (file.getParentFile() != null) {
            file.getParentFile().mkdirs();
        }

        PrintStream ps = null;
        try {
            file.createNewFile();
            ps = new PrintStream(new FileOutputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Application app : Application.app_list) {
            for (Instance inst : app.inst_list) {
                ps.print(app.id + "," + inst.getTarHost().getID() + "\n");
            }
        }

        ps.close();
    }

    public static void load(String path) {
        Scanner sc = null;
        try {
            sc = new Scanner(new File(path));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (sc.hasNextLine()) {
            String[] s = sc.nextLine().split(",");

            Application app = Application.getApp(Integer.parseInt(s[0]));
            Machine m = Machine.getMachine(Integer.parseInt(s[1]));

            app.getInst(null).setTarHost(m);
            m.add(app);
        }

        sc.close();
    }
}

