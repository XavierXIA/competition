/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package placement;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import model.app.Application;
import model.app.Instance;
import model.infra.Machine;
import control.Main;

final class HeuristicSearcher {
    private Map<Machine, Double> machine_fit_map;
    private Machine updatedMachine;

    public HeuristicSearcher() {
        machine_fit_map = new HashMap<>();
        updatedMachine = null;
    }

    public void search() {
        // phase 1 : unstable apps
        int i = 0;
        Application app = Application.app_list.get(i);
        while (!app.isStable) {
            boolean isFirstInst = true;
            for (int j = app.inst_list.size() - 1; j >= 0; j--) {
                Instance inst = app.inst_list.get(j);
                if (!inst.isPlaced()) {
                    if (isFirstInst) {
                        place(inst, app, j > 0);
                        isFirstInst = false;
                    } else {
                        place(inst, app);
                    }
                }
            }

            machine_fit_map.clear();

            i++;
            app = Application.app_list.get(i);
        }

        // phase 2 : stable apps
        Machine.updateCriteria(Machine.CriterMode.DIS2MAX_CPU_CON);
        Collections.sort(Machine.considerMachines, Machine.ascendCompare);

        for (int appNB = Application.app_list.size(); i < appNB; i++) {
            app = Application.app_list.get(i);
            for (Instance inst : app.inst_list) {
                if (!inst.isPlaced()) {
                    int j;
                    Machine m = null;
                    for (j = Machine.considerMachines.size() - 1; j >= 0; j--) {
                        m = Machine.considerMachines.get(j);
                        if (m.isConformSoftConstr(app)) {
                            m.add(app);
                            inst.setTarHost(m);

                            break;
                        }
                    }

                    if (!inst.isPlaced()) {
                        m = null;
                        for (j = Machine.considerMachines.size() - 1; j >= 0; j--) {
                            m = Machine.considerMachines.get(j);
                            if (m.isConformHardConstr(app)) {
                                m.add(app);
                                inst.setTarHost(m);

                                break;
                            }
                        }
                    }

                    assert inst.isPlaced() : "Placer : fail to place an instance !";

                    m.updateDis2MaxCpuCon();
                    if (j > 0) {
                        Machine.considerMachines.remove(j);
                        Machine.considerMachines.add(getIndexWrtDis(m.getDis2MaxCpuCon(), j), m);
                    }
                }
            }
        }

        if (Main.debug) {
            assert Machine.getHostedInstCount() == Application.getInstCount() : "Placer : Hosted inst count does not equal to total inst count!";
            assert Application.getHostedInstCount() == Application.getInstCount() : "Placer : Hosted inst count does not equal to total inst count!";
            for (Application a : Application.app_list) {
                for (Instance inst : a.inst_list) {
                    assert inst.isPlaced() : "Placer : there exists unplaced instance !";
                }
            }
            for (Machine m : Machine.getAllMachines()) {
                m.debug();
            }
        }
    }

    private int getIndexWrtDis(double d, int upper) {
        int start = 0;
        if (Machine.considerMachines.get(start).getDis2MaxCpuCon() >= d) {
            return start;
        }

        int end = upper - 1;
        if (Machine.considerMachines.get(end).getDis2MaxCpuCon() <= d) {
            return upper;
        }

        int index;
        while (end > start + 1) {
            index = Math.max((start + end) >> 1, start + 1);

            if (d > Machine.considerMachines.get(index).getDis2MaxCpuCon()) {
                start = index;
            } else {
                end = index;
            }
        }

        return end;
    }

    private void place(Instance inst, Application app, boolean isStore) {
        updatedMachine = null;
        double minFit = Double.MAX_VALUE;
        double bestCpuDev = Double.MAX_VALUE;
        double meanCPU, cpuDev;
        for (Machine m : Machine.considerMachines) {
            if (m.isEmpty()) {
                if (updatedMachine == null) {
                    assert m.isConformHardConstr(app) : "Placer : empty machine can not host an instance !";
                    updatedMachine = m;
                }
                break;
            } else {
                meanCPU = m.testAddMeanAvCPU(app);
                if (m.isConformSoftConstr(app, meanCPU)) {
                    cpuDev = m.testAddCpuDev(app, meanCPU);
                    if (cpuDev < Double.MAX_VALUE) {
                        double fitness = FitnessCalculator.getFitness(app, m, cpuDev);

                        if (isStore) {
                            machine_fit_map.put(m, fitness);
                        }

                        if (fitness < minFit) {
                            updatedMachine = m;
                            bestCpuDev = cpuDev;
                            minFit = fitness;
                        }
                    }
                }
            }
        }

        assert updatedMachine != null : "Placer : fail to place an instance !";

        updatedMachine.add(app);
        inst.setTarHost(updatedMachine);

        if (bestCpuDev < Double.MAX_VALUE) {
            updatedMachine.setCpuDev(bestCpuDev);
        } else {
            updatedMachine.updateCpuDev();
        }
    }

    // based on machine_fit_map
    private void place(Instance inst, Application app) {
        assert updatedMachine != null : "Placer : no updated machine !";

        machine_fit_map.remove(updatedMachine);

        double meanCPU = updatedMachine.testAddMeanAvCPU(app);
        if (updatedMachine.isConformSoftConstr(app, meanCPU)) {
            double cpuDev = updatedMachine.testAddCpuDev(app, meanCPU);
            if (cpuDev < Double.MAX_VALUE) {
                double fitness = FitnessCalculator.getFitness(app, updatedMachine, cpuDev);
                machine_fit_map.put(updatedMachine, fitness);
            }
        }

        updatedMachine = null;
        double min = Double.MAX_VALUE;
        for (Entry<Machine, Double> entry : machine_fit_map.entrySet()) {
            if (min > entry.getValue()) {
                updatedMachine = entry.getKey();
            }
        }

        if (updatedMachine == null) {
            for (Machine m : Machine.considerMachines) {
                if (m.isEmpty()) {
                    assert m.isConformHardConstr(app) : "Placer : empty machine can not host an instance !";
                    updatedMachine = m;
                    break;
                }
            }
        }

        assert updatedMachine != null : "Placer : fail to place an instance !";

        updatedMachine.add(app);
        inst.setTarHost(updatedMachine);
        updatedMachine.updateCpuDev();
    }
}

