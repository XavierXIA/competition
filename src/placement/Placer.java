/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package placement;

import improvement.global.HostTemplater;
import improvement.global.MachineSealer;
import improvement.local.LocalImprover;
import model.app.Application;
import model.app.Instance;
import model.infra.Machine;

import common.Printer;

import control.Main;

public final class Placer {
    public void place(int roundNB) {
        HeuristicSearcher searcher = new HeuristicSearcher();
        LocalImprover improver = new LocalImprover();

        for (int i = 0; i<roundNB; i++) {
            if (HostTemplater.isSeal) {
                MachineSealer.clear();
            }

            Machine.resetAll();
            Instance.resetAll();

            new PreProcessor(Application.AppOrderMode.VAR_CPU).preprocess();

            searcher.search();

            // if (Utils.nextBoolean()) {
            // } else {
            // orderMode = "MAX_MIN_DIS";
            // placer = new Placer(Application.AppOrderMode.MAX_MIN_DIS);
            // }

            int count = 0;
            for (Machine m : Machine.getAllMachines()) {
                if (!m.isEmpty()) {
                    count++;
                }
            }
            Machine.setOccupiedMachineNB(count);

            if (Main.debug) {
                double score = 0;
                for (Machine m : Machine.getAllMachines()) {
                    score += m.getScore();
                }

                Printer.info("VAR_CPU, score : " + score);
                Printer.pass("machine nb : " + Machine.getOccupiedMachineNB());
            }

            improver.improve(1);

            if (Main.isPrint) {
                double score = 0;
                for (Machine m : Machine.getAllMachines()) {
                    score += m.getScore();
                }

                Printer.info("round " + i + ", score : " + score);
//                PlaceIO_Manager.save(i + ".csv");
            }

            HostTemplater.testAddTemplates4cpu();
            HostTemplater.incrThreshold();
        }

        if (Main.debug) {
            for (Machine m : Machine.considerMachines) {
                if (!m.isEmpty()) {
                    Printer.print(m.type.capCPU + " : " + m.getDis2ExpectedCPU());
                }
            }
        }

        HostTemplater.clear();
        // Machine.analyse();
    }
}

