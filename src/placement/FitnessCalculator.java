/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package placement;

import model.app.Application;
import model.infra.Machine;

import common.Utils;

//TODO : stable app fit
public final class FitnessCalculator {
    private static double para = 1;

    public static double getFitness(Application app, Machine m, double cpuDev) {
        return cpuDev - m.getCpuDev();
//        return para * (cpuDev - m.getCpuDev()) + (1 - para) * (cpuDev - m.getIncrCpuDevCeil(app));
    }

    public static double randomCriteriaPara() {
        para = Utils.nextDouble();
        return para;
    }
}

