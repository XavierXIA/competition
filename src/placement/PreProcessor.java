/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package placement;

import improvement.global.HostTemplater;
import improvement.global.ParaSetter;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import model.app.Application;
import model.app.Instance;
import model.infra.Machine;
import control.Main;

//TODO : imbalanced
//TODO : not generic
public final class PreProcessor {
    private static final double cpuDevVarThreshold = 0;

    private static int memCeil = 55; // {50, 60, 64}
    private static int diskCeil = 5000; // {300, 500, 600}
    private static int maxCpuCeil = 16; // {20, 21}
    private static int meanCpuCeil = 16; // {17, 18, 19}

    private Map<Machine, Double> machine_fit_map;
    private int count;

    public PreProcessor(Application.AppOrderMode mode) {
        machine_fit_map = new HashMap<>();

        Application.updateAppOrder(mode);
        Collections.sort(Machine.considerMachines, new Comparator<Machine>() {
            @Override
            public int compare(Machine m1, Machine m2) {
                return m1.type.capCPU < m2.type.capCPU ? 1 : m1.type.capCPU == m2.type.capCPU ? 0 : -1;
            }
        });
    }

    // doubled ?
    // try to balance
    public void preprocess() {
        int nb = HostTemplater.loadTemplates();

        Machine.updateCriteria(Machine.CriterMode.CPU_DEV);
        Comparator<Instance> comparator = new Comparator<Instance>() {
            @Override
            public int compare(Instance inst1, Instance inst2) {
                if (inst1.isPlaced() && !inst2.isPlaced()) {
                    return 1;
                } else if (!inst1.isPlaced() && inst2.isPlaced()) {
                    return -1;
                }

                return 0;
            }
        };
        for (Application app : Application.app_list) {
            Collections.sort(app.inst_list, comparator);
        }

        count = 0;
        for (Application app : Application.app_list) {
            if (app.maxMEM >= memCeil || app.reqDISK >= diskCeil || app.maxCPU > maxCpuCeil
                    || app.meanCPU > meanCpuCeil) {
                for (Instance inst : app.inst_list) {
                    if (!inst.isPlaced()) {
                        Machine m = Machine.considerMachines.get(count);
                        m.add(app);
                        inst.setTarHost(m);

                        m.updateCpuDev();
                        count++;
                    }
                }
            }
        }

        occupyMachines(ParaSetter.getExpectedNB() - nb);

        if (Main.debug) {
            for (Machine machine : Machine.getAllMachines()) {
                machine.debug();
            }
        }
    }

    private void occupyMachines(int machineNB) {
        for (Application app : Application.app_list) {
            boolean isFirstInst = true, isStore = false;
            Machine m = null;
            for (int i = app.inst_list.size() - 1; i >= 0; i--) {
                Instance inst = app.inst_list.get(i);
                if (!inst.isPlaced()) {
                    if (isFirstInst) {
                        isFirstInst = false;
                        isStore = i > 0;
                        m = getBestFitMachine(app, isStore);
                    } else {
                        machine_fit_map.remove(m);
                        double d = m.testAddMeanAvCPU(app);
                        if (m.isConformSoftConstr(app, d)) {
                            double cpuDevVar = m.testAddCpuDev(app, d) - m.getCpuDev();
                            if (cpuDevVar < cpuDevVarThreshold) {
                                machine_fit_map.put(m, cpuDevVar);
                            }
                        }

                        m = null;
                        d = Double.MAX_VALUE;
                        for (Entry<Machine, Double> entry : machine_fit_map.entrySet()) {
                            if (d > entry.getValue()) {
                                m = entry.getKey();
                            }
                        }
                    }

                    if (m == null) {
                        count++;
                        m = Machine.considerMachines.get(count);
                        assert m.isConformHardConstr(app) : "PreProcessor : empty machine can not host an instance !";

                        m.add(app);
                        inst.setTarHost(m);
                        m.updateCpuDev();

                        if (count >= machineNB) {
                            machine_fit_map.clear();
                            return;
                        }
                    } else {
                        m.add(app);
                        inst.setTarHost(m);
                        if (isStore) {
                            m.setCpuDev(machine_fit_map.get(m) + m.getCpuDev());
                        } else {
                            m.updateCpuDev();
                        }
                    }
                }
            }

            machine_fit_map.clear();
        }
    }

    private Machine getBestFitMachine(Application app, boolean isStore) {
        Machine m, result = null;
        double meanCPU, cpuDevVar, min = cpuDevVarThreshold;
        for (int i = 0; i < count; i++) {
            m = Machine.considerMachines.get(i);
            meanCPU = m.testAddMeanAvCPU(app);
            if (m.isConformSoftConstr(app, meanCPU)) {
                cpuDevVar = m.testAddCpuDev(app, meanCPU) - m.getCpuDev();

                if (isStore && cpuDevVar < cpuDevVarThreshold) {
                    machine_fit_map.put(m, cpuDevVar);
                }

                if (min > cpuDevVar) {
                    min = cpuDevVar;
                    result = m;
                }
            }
        }

        return result;
    }

    public static void setMemCeil(int i) {
        memCeil = i;
    }

    public static void setDiskCeil(int i) {
        diskCeil = i;
    }

    public static void setMaxCpuCeil(int maxCpuCeil) {
        PreProcessor.maxCpuCeil = maxCpuCeil;
    }

    public static void setMeanCpuCeil(int meanCpuCeil) {
        PreProcessor.meanCpuCeil = meanCpuCeil;
    }
}

