/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package scheduling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import model.app.Application;
import model.app.Instance;
import model.infra.Machine;
import model.infra.MachineType;

final class PlacementAdjuster {
    private int[] stayBonus;

    public PlacementAdjuster() {
        stayBonus = new int[Machine.totalMachineNB];
    }

    public void adjust() {
        swapHostingFlavor();
        swapInsts();

        boolean isUpdated = true;
        while (isUpdated) {
            while (isUpdated) {
                isUpdated = swapMachines();
                isUpdated |= swapInsts();

                // TODO : test
                // isUpdated = swapInsts();
            }

            isUpdated = adjustNoInitHost(true);
        }

        stayBonus = null;
    }

    public void swapHostingFlavor() {
        Map<MachineType, Set<Machine>> type2machines = new HashMap<>();
        for (MachineType type : MachineType.type_list) {
            type2machines.put(type, new HashSet<Machine>());
        }
        for (Machine m : Machine.considerMachines) {
            type2machines.get(m.type).add(m);
        }

        for (Set<Machine> machines : type2machines.values()) {
            for (Machine src : machines) {
                for (Machine tar : machines) {
                    int sum = 0;
                    for (Entry<Application, Integer> entry : src.getHostedApps().entrySet()) {
                        int count = 0;
                        for (Instance inst : entry.getKey().inst_list) {
                            if (inst.getInitHostID() == tar.getID()) {
                                count++;
                            }
                        }

                        count = Math.min(count, entry.getValue());
                        sum += count;
                    }

                    if (sum > 0) {
                        FlavorSwapWrapper.add(src, tar.getID(), sum);
                    }
                }
            }
            FlavorSwapWrapper.swap();
        }
    }

    private boolean swapMachines() {
        boolean isUpdated = false;

        Set<Machine> swapped = new HashSet<>();
        int machineNB = Machine.totalMachineNB;

        do {
            swapped.clear();

            for (int i = 0; i < machineNB; i++) {
                stayBonus[i] = 0;
            }

            for (Application app : Application.app_list) {
                for (Instance inst : app.inst_list) {
                    if (inst.hasInitHost()) {
                        Machine tarHost = inst.getTarHost();
                        if (tarHost.getID() == inst.getInitHostID()) {
                            stayBonus[tarHost.index]++;
                        } else {
                            Machine initHost = Machine.getMachine(inst.getInitHostID());
                            if (initHost.type == tarHost.type) {
                                MachineSwapWrapper.addSwap(initHost, tarHost);
                            }
                        }
                    }
                }
            }

            for (MachineSwapWrapper w : MachineSwapWrapper.getSwaps()) {
                if (!swapped.contains(w.m1) && !swapped.contains(w.m2)
                        && w.isValid(stayBonus[w.m1.index] + stayBonus[w.m2.index])) {
                    w.m1.swap(w.m2);

                    swapped.add(w.m1);
                    swapped.add(w.m2);

                    isUpdated = true;
                }
            }
            MachineSwapWrapper.clearMap();
        } while (!swapped.isEmpty());

        return isUpdated;
    }

    private boolean swapInsts() {
        boolean isUpdated = false;

        for (Application app : Application.app_list) {
            for (Instance srcInst : app.inst_list) {
                int initHostID = srcInst.getInitHostID();
                if (srcInst.hasInitHost() && initHostID != srcInst.getTarHost().getID()) {
                    for (Instance tarInst : app.inst_list) {
                        int tarHostID = tarInst.getTarHost().getID();
                        if (tarHostID == initHostID && tarInst.getInitHostID() != tarHostID) {
                            srcInst.swap(tarInst);

                            isUpdated = true;
                            break;
                        }
                    }
                }
            }
        }

        return isUpdated;
    }

    private boolean adjustNoInitHost(boolean isStayBonusInitialized) {
        boolean isUpdated = false;

        if (!isStayBonusInitialized) {
            for (int i = 0; i < Machine.totalMachineNB; i++) {
                stayBonus[i] = 0;
            }

            for (Application app : Application.app_list) {
                for (Instance inst : app.inst_list) {
                    if (inst.hasInitHost()) {
                        Machine tarHost = inst.getTarHost();
                        if (tarHost.getID() == inst.getInitHostID()) {
                            stayBonus[tarHost.index]++;
                        }
                    }
                }
            }
        }

        for (Application app : Application.app_list) {
            for (Instance srcInst : app.inst_list) {
                if (srcInst.hasInitHost() && srcInst.getInitHostID() != srcInst.getTarHost().getID()) {
                    Machine initHost = Machine.getMachine(srcInst.getInitHostID());
                    if (stayBonus[initHost.index] == 0) {
                        for (Instance tarInst : app.inst_list) {
                            Machine candidate = tarInst.getTarHost();
                            if (stayBonus[candidate.index] == 0 && initHost.type == candidate.type) {
                                srcInst.swap(tarInst);
                                initHost.swap(candidate);

                                stayBonus[initHost.index]++;

                                isUpdated = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        return isUpdated;
    }
}

class FlavorSwapWrapper {
    private static final List<FlavorSwapWrapper> wrappers = new ArrayList<>();
    private static FlavorSwapWrapper curr = null;

    public final Machine src;
    public final int tarID;
    private int count;

    private static final Comparator<FlavorSwapWrapper> comparator = new Comparator<FlavorSwapWrapper>() {
        @Override
        public int compare(FlavorSwapWrapper w1, FlavorSwapWrapper w2) {
            return w1.count > w2.count ? -1 : w1.count == w2.count ? 0 : 1;
        }
    };

    private FlavorSwapWrapper(Machine s, int t, int i) {
        src = s;
        tarID = t;
        count = i;
    }

    public static void add(Machine src, int tarID, int i) {
        curr = new FlavorSwapWrapper(src, tarID, i);
        wrappers.add(curr);
    }

    public static void swap() {
        Collections.sort(wrappers, comparator);

        while (!wrappers.isEmpty()) {
            FlavorSwapWrapper next = next();
            next.src.swap(Machine.getMachine(next.tarID));

            Iterator<FlavorSwapWrapper> iter = wrappers.iterator();
            while (iter.hasNext()) {
                FlavorSwapWrapper w = iter.next();
                if (w.src == next.src || w.tarID == next.tarID) {
                    iter.remove();
                }
            }
        }
    }

    private static FlavorSwapWrapper next() {
        Map<Machine, Integer> srcMap = new HashMap<>();
        Map<Integer, Integer> tarMap = new HashMap<>();

        int count = wrappers.get(0).count;
        for (FlavorSwapWrapper w : wrappers) {
            if (w.count < count) {
                break;
            }

            if (srcMap.containsKey(w.src)) {
                srcMap.put(w.src, srcMap.get(w.src) + 1);
            } else {
                srcMap.put(w.src, 1);
            }

            if (tarMap.containsKey(w.tarID)) {
                tarMap.put(w.tarID, tarMap.get(w.tarID) + 1);
            } else {
                tarMap.put(w.tarID, 1);
            }
        }

        FlavorSwapWrapper result = null;
        int min = Integer.MAX_VALUE;
        for (FlavorSwapWrapper w : wrappers) {
            if (w.count < count) {
                break;
            }

            int sum = srcMap.get(w.src) + tarMap.get(w.tarID);
            if (min > sum) {
                min = sum;
                result = w;
            }
        }

        return result;
    }
}

class MachineSwapWrapper {
    private static final Map<Integer, MachineSwapWrapper> id_wrapper_map = new HashMap<>();
    private static final Comparator<MachineSwapWrapper> comparator = new Comparator<MachineSwapWrapper>() {
        @Override
        public int compare(MachineSwapWrapper w1, MachineSwapWrapper w2) {
            return w1.count < w2.count ? 1 : w1.count == w2.count ? 0 : -1;
        }
    };

    public final Machine m1;
    public final Machine m2;
    private int count;

    private MachineSwapWrapper(Machine m1, Machine m2) {
        if (m1.index < m2.index) {
            this.m1 = m1;
            this.m2 = m2;
        } else {
            this.m2 = m1;
            this.m1 = m2;
        }

        count = 1;
    }

    public static void addSwap(Machine m1, Machine m2) {
        int id;
        if (m1.index < m2.index) {
            id = m1.index * Machine.totalMachineNB + m2.index;
        } else {
            id = m2.index * Machine.totalMachineNB + m1.index;
        }

        if (id_wrapper_map.containsKey(id)) {
            id_wrapper_map.get(id).count++;
        } else {
            id_wrapper_map.put(id, new MachineSwapWrapper(m1, m2));
        }
    }

    public static void clearMap() {
        id_wrapper_map.clear();
    }

    public static List<MachineSwapWrapper> getSwaps() {
        List<MachineSwapWrapper> result = new ArrayList<>(id_wrapper_map.values());
        Collections.sort(result, comparator);

        return result;
    }

    public boolean isValid(int stayBonus) {
        return count > stayBonus;
    }
}
