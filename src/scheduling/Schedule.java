/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package scheduling;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.app.Instance;
import model.infra.Machine;

import common.Printer;

import control.Main;

public final class Schedule {
    private final List<Integer> instID_list;
    private final List<Integer> machineID_list;

    private double score;

    public Schedule(double s) {
        instID_list = new ArrayList<>();
        machineID_list = new ArrayList<>();

        score = s;
    }

    public void append(int instIntID, int machineIntID) {
        instID_list.add(instIntID);
        machineID_list.add(machineIntID);
    }

    public double getScore() {
        return score;
    }

    public void incrScore(double s) {
        score += s;
    }

    public void save() throws IOException {
        SimpleDateFormat format = new SimpleDateFormat("YYMMdd_HHmmss");
        String path = "submit/submit_" + format.format(new Date()) + ".csv";

        if (Main.isPrint || Main.debug) {
            Printer.info("generated schedule : " + path);
        }

        File file = new File(path);
        if (file.getParentFile() != null) {
            file.getParentFile().mkdirs();
        }
        file.createNewFile();

        PrintStream ps = new PrintStream(new FileOutputStream(file));

        int optNB = instID_list.size();
        for (int i = 0; i < optNB; i++) {
            ps.print(Instance.prefix + instID_list.get(i) + "," + Machine.prefix
                    + machineID_list.get(i) + "\n");
        }

        ps.close();
    }
}

