/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package scheduling;

import improvement.global.MachineSealer;

import java.text.DecimalFormat;

import model.app.Application;
import model.app.Instance;
import model.app.Interference;
import model.infra.Machine;

import common.Printer;
import common.Utils;

import control.Main;

public final class Scheduler {
    private Schedule schedule;

    public Scheduler() {
        MachineSealer.clear();

        long startTime = Main.debug ? System.nanoTime() : 0;

        new PlacementAdjuster().adjust();

        if (Main.debug) {
            DecimalFormat formatter = new DecimalFormat("#.###");
            Printer.info("Adjust time : " + formatter.format((System.nanoTime() - startTime) / 6e10));
        }

        Machine.resetAll();
        Instance.initCurrHosts();
        for (Application app : Application.app_list) {
            for (Instance inst : app.inst_list) {
                if (inst.hasInitHost()) {
                    inst.getCurrHost().add(app);
                }
            }
        }

        double score = 0;
        for (Machine m : Machine.getAllMachines()) {
            score += m.getScore();
        }
        schedule = new Schedule(score);
    }

    public Schedule getSchedule() {
        boolean finish = false;
        Machine curr, tar;

        int migrateCount = 0;
        int additionalCount = 0;
        while (!finish) {
            finish = true;
            boolean blocked = true;

            for (Application app : Application.app_list) {
                for (Instance inst : app.inst_list) {
                    curr = inst.getCurrHost();
                    tar = inst.getTarHost();
                    if (inst.hasInitHost() && curr != tar) {
                        finish = false;

                        if (tar.isConformScheduleConstr(app)) {
                            curr.remove(app);
                            tar.add(app);
                            inst.setCurrHost(tar);

                            schedule.append(inst.getID(), tar.getID());

                            if (Main.debug) {
                                migrateCount++;
                            }

                            blocked = false;
                        }
                    }
                }
            }

            // TODO : to improve
            if (blocked && !finish) {
                Machine m = mostPopularMachine();

                Application nextApp = null;
                Instance nextInst = null;
                int maxInterfNB = -1;
                for (Application app : m.getHostedApps().keySet()) {
                    for (Instance inst : app.inst_list) {
                        if (inst.hasInitHost() && inst.getCurrHost() != inst.getTarHost()) {
                            if (inst.getCurrHost() == m && maxInterfNB < Interference.getInterfNB(app)) {
                                maxInterfNB = Interference.getInterfNB(app);
                                nextApp = app;
                                nextInst = inst;
                            }
                        }
                    }
                }

                Machine intermediate;
                do {
                    intermediate = Machine.considerMachines.get(Utils.nextInt(Machine.considerMachines.size()));
                } while (!intermediate.isConformScheduleConstr(nextApp));

                nextInst.getCurrHost().remove(nextApp);
                intermediate.add(nextApp);
                nextInst.setCurrHost(intermediate);

                schedule.append(nextInst.getID(), intermediate.getID());

                if (Main.debug) {
                    migrateCount++;
                    additionalCount++;
                }
            }
        }

        if (Main.debug) {
            Printer.print("migration count : " + migrateCount);
            Printer.print("additional migration count : " + additionalCount);
        }

        // undeployed instances
        for (Application app : Application.app_list) {
            for (Instance instance : app.inst_list) {
                if (!instance.hasInitHost()) {
                    schedule.append(instance.getID(), instance.getTarHost().getID());

                    if (Main.debug) {
                        instance.setCurrHost(instance.getTarHost());
                        instance.getTarHost().add(app);
                    }
                }
            }
        }

        if (Main.debug) {
            for (Application app : Application.app_list) {
                for (Instance inst : app.inst_list) {
                    assert inst.getCurrHost() == inst.getTarHost() : "Scheduler : there exists unscheduled instance !";
                }
            }
            for (Machine m : Machine.getAllMachines()) {
                m.debug();
            }
        }

        return schedule;
    }

    private Machine mostPopularMachine() {
        int[] popularity = new int[Machine.totalMachineNB];
        for (int i = 0; i < Machine.totalMachineNB; i++) {
            popularity[i] = 0;
        }
        int max = 0;
        Machine result = null;
        int index = -1;

        for (Application app : Application.app_list) {
            for (Instance inst : app.inst_list) {
                if (inst.hasInitHost() && inst.getCurrHost() != inst.getTarHost()) {
                    index = inst.getTarHost().index;
                    popularity[index]++;

                    if (popularity[index] > max) {
                        max = popularity[index];
                        result = inst.getTarHost();
                    }
                }
            }
        }

        assert result != null : "Scheduler : empty machine obtained !";
        return result;
    }
}

