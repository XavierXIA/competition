/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package model.infra;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import model.app.Application;
import model.app.Instance;
import model.app.Interference;

import common.Printer;
import common.Utils;

public final class Machine {
    public static final String prefix = "machine_";
    // TODO : not generic
    public static final int timeSlotNB = 98;
    public static final int totalMachineNB = 6000;
    public static final List<Machine> considerMachines = new ArrayList<>(totalMachineNB);
    private static final Map<Integer, Machine> id2machine = new HashMap<>(4096);

    private static int occupiedMachineNB;

    private int id;
    private final double[] avCPU;
    private final double[] avMEM;
    private int avDISK;
    private int avP;
    private int avM;
    private int avPM;

    public final MachineType type;
    public final int index;

    private Map<Application, Integer> hostedApps;

//    SCORE,
    public static enum CriterMode {
        CPU_DEV, CPU_LOAD, DIS2MAX_CPU_CON
    };

    private static CriterMode criterMode;
    private double criterion;

    public static final Comparator<Machine> ascendCompare = new Comparator<Machine>() {
        @Override
        public int compare(Machine m1, Machine m2) {
            return m1.criterion > m2.criterion ? 1 : m1.criterion == m2.criterion ? 0 : -1;
        }
    };
    public static final Comparator<Machine> descendCompare = new Comparator<Machine>() {
        @Override
        public int compare(Machine m1, Machine m2) {
            return m1.criterion < m2.criterion ? 1 : m1.criterion == m2.criterion ? 0 : -1;
        }
    };

    public Machine(String s, String config, int index) {
        id = string2ID(s);
        this.index = index;
        id2machine.put(id, this);
        considerMachines.add(this);

        type = MachineType.getMachineType(config);

        avCPU = new double[timeSlotNB];
        for (int i = 0; i < timeSlotNB; i++) {
            avCPU[i] = type.capCPU;
        }
        avMEM = new double[timeSlotNB];
        for (int i = 0; i < timeSlotNB; i++) {
            avMEM[i] = type.capMEM;
        }

        avDISK = type.capDISK;
        avP = type.capP;
        avM = type.capM;
        avPM = type.capPM;

        hostedApps = new HashMap<>();
        criterion = 0;
    }

    public static void updateCriteria(CriterMode mode) {
        if (mode == criterMode) {
            return;
        }

        switch (mode) {
//            case SCORE:
//                for (Machine m : considerMachines) {
//                    m.criterion = m.getScore();
//                }
//                break;
            case CPU_DEV:
                for (Machine m : considerMachines) {
                    m.criterion = m.getCpuDev();
                }
                break;
            case CPU_LOAD:
                for (Machine m : considerMachines) {
                    m.criterion = 0;
                    for (double cpu : m.avCPU) {
                        m.criterion += cpu;
                    }

                    m.criterion = m.type.capCPU * m.avCPU.length - m.criterion;
                }
                break;
            case DIS2MAX_CPU_CON:
                for (Machine m : considerMachines) {
                    m.criterion = m.getDis2MaxCpuCon();
                }
                break;
            default:
                Printer.err("Machine : Unexpected CriterMode !");
        }

        criterMode = mode;
    }

    public void add(Application app) {
        for (int i = 0; i < timeSlotNB; i++) {
            avCPU[i] -= app.reqCPU[i];
            avMEM[i] -= app.reqMEM[i];
        }

        avDISK -= app.reqDISK;
        avP -= app.reqP;
        avM -= app.reqM;
        avPM -= app.reqPM;

        if (hostedApps.containsKey(app)) {
            hostedApps.put(app, hostedApps.get(app) + 1);
        } else {
            hostedApps.put(app, 1);
        }
    }

    public void remove(Application app) {
        assert hostedApps.containsKey(app) : "Machine : try to deplace unplaced app !";

        int remainedNB = hostedApps.get(app) - 1;
        if (remainedNB == 0) {
            hostedApps.remove(app);
        } else {
            hostedApps.put(app, remainedNB);
        }

        for (int i = 0; i < timeSlotNB; i++) {
            avCPU[i] += app.reqCPU[i];
            avMEM[i] += app.reqMEM[i];
        }

        avDISK += app.reqDISK;
        avP += app.reqP;
        avM += app.reqM;
        avPM += app.reqPM;
    }

    // return true if valid
    public boolean isConformHardConstr(Application app) {
        if (avDISK < app.reqDISK || avP < app.reqP || avM < app.reqM || avPM < app.reqPM
                || Interference.isInterfered(hostedApps, app)) {
            return false;
        }

        for (int i = 0; i < timeSlotNB; i++) {
            if (app.reqCPU[i] - avCPU[i] > Utils.EPSILON || app.reqMEM[i] - avMEM[i] > Utils.EPSILON) {
                return false;
            }
        }

        return true;
    }

    // return true if valid
    public boolean isConformSoftConstr(Application app) {
        if (avDISK < app.reqDISK || avP < app.reqP || avM < app.reqM || avPM < app.reqPM
                || Interference.isInterfered(hostedApps, app)) {
            return false;
        }

        for (int i = 0; i < timeSlotNB; i++) {
            if (avCPU[i] - app.reqCPU[i] < type.getMinAvCPU() || app.reqMEM[i] - avMEM[i] > Utils.EPSILON) {
                return false;
            }
        }

        double sum = 0;
        for (int i = 0; i < timeSlotNB; i++) {
            sum += avCPU[i] - app.reqCPU[i];
        }
        if (sum / timeSlotNB < type.getMinMeanAvCPU()) {
            return false;
        }

        return true;
    }

    // return true if valid
    public boolean isConformSoftConstr(Application app, double meanCPU) {
        if (meanCPU < type.getMinMeanAvCPU()) {
            return false;
        }

        if (avDISK < app.reqDISK || avP < app.reqP || avM < app.reqM || avPM < app.reqPM
                || Interference.isInterfered(hostedApps, app)) {
            return false;
        }

        for (int i = 0; i < timeSlotNB; i++) {
            if (avCPU[i] - app.reqCPU[i] < type.getMinAvCPU() || app.reqMEM[i] - avMEM[i] > Utils.EPSILON) {
                return false;
            }
        }

        return true;
    }

    // return true if valid
    public boolean isConformScheduleConstr(Application app) {
        if (avDISK < app.reqDISK || avP < app.reqP || avM < app.reqM || avPM < app.reqPM
                || Interference.isHardInterfered(hostedApps, app)) {
            return false;
        }

        for (int i = 0; i < timeSlotNB; i++) {
            if (app.reqCPU[i] - avCPU[i] > Utils.EPSILON || app.reqMEM[i] - avMEM[i] > Utils.EPSILON) {
                return false;
            }
        }

        return true;
    }

    public double testAddMeanAvCPU(Application app) {
        double sum = 0;
        for (int i = 0; i < timeSlotNB; i++) {
            sum += avCPU[i] - app.reqCPU[i];
        }
        return sum / timeSlotNB;
    }

    public double testAddCpuDev(Application app, double meanCPU) {
        assert criterMode == CriterMode.CPU_DEV : "Machine : unexpected CriterMode !";

        double result = 0;
        for (int i = 0; i < timeSlotNB; i++) {
            result += Math.abs(avCPU[i] - app.reqCPU[i] - meanCPU);
        }

        result = result / timeSlotNB / type.capCPU;

        double cpuDevCeil = type.getCpuDevCeil(meanCPU);
        if (result > cpuDevCeil) {
            return Double.MAX_VALUE;
        }

        if (result - criterion > Utils.EPSILON && criterion > type.getIncrCpuDevCeil(cpuDevCeil)) {
            return Double.MAX_VALUE;
        }

        return result;
    }

    public double testAddScore(Application app) {
        double score = 0;
        for (int i = 0; i < timeSlotNB; i++) {
            score += 1 + 10 * (Math.exp(Math.max(0, 0.5 - (avCPU[i] - app.reqCPU[i]) / type.capCPU)) - 1);
        }
        return score / timeSlotNB;
    }

    public double testRmScore(Application app) {
        if (hostedApps.get(app) == 1 && hostedApps.size() == 1) {
            return 0;
        }

        double score = 0;
        for (int i = 0; i < timeSlotNB; i++) {
            score += 1 + 10 * (Math.exp(Math.max(0, 0.5 - (avCPU[i] + app.reqCPU[i]) / type.capCPU)) - 1);
        }
        return score / timeSlotNB;
    }

    public double testAddDis2ExpectedCPU(Application app) {
        double dis = 0;

        for (int i = 0; i < timeSlotNB; i++) {
            if (MachineType.isConsiderNegDis[i]) {
                dis += Math.abs(avCPU[i] - app.reqCPU[i] - type.expectedAvCpu[i]);
            } else {
                dis += Math.max(type.expectedAvCpu[i] - avCPU[i] + app.reqCPU[i], 0);
            }
        }

        return dis;
    }

    public double testRmDis2ExpectedCPU(Application app) {
        double dis = 0;

        for (int i = 0; i < timeSlotNB; i++) {
            if (MachineType.isConsiderNegDis[i]) {
                dis += Math.abs(avCPU[i] + app.reqCPU[i] - type.expectedAvCpu[i]);
            } else {
                dis += Math.max(type.expectedAvCpu[i] - avCPU[i] - app.reqCPU[i], 0);
            }
        }

        return dis;
    }

    public double testExDis2ExpectedCPU(Application app2add, Application app2rm) {
        double dis = 0;

        for (int i = 0; i < timeSlotNB; i++) {
            if (MachineType.isConsiderNegDis[i]) {
                dis += Math.abs(avCPU[i] - app2add.reqCPU[i] + app2rm.reqCPU[i] - type.expectedAvCpu[i]);
            } else {
                dis += Math.max(type.expectedAvCpu[i] - avCPU[i] - app2rm.reqCPU[i] + app2add.reqCPU[i], 0);
            }
        }

        return dis;
    }

    public double getDis2ExpectedCPU() {
        double dis = 0;

        for (int i = 0; i < timeSlotNB; i++) {
            if (MachineType.isConsiderNegDis[i]) {
                dis += Math.abs(avCPU[i] - type.expectedAvCpu[i]);
            } else {
                dis += Math.max(type.expectedAvCpu[i] - avCPU[i], 0);
            }
        }

        return dis;
    }

    public double getScore() {
//        if (criterMode == CriterMode.SCORE) {
//            return criterion;
//        }

        if (isEmpty()) {
            return 0;
        }

        double score = 0;
        for (double cpu : avCPU) {
            score += 1 + 10 * (Math.exp(Math.max(0, 0.5 - cpu / type.capCPU)) - 1);
        }
        return score / timeSlotNB;
    }

    // TODO
    public double getIncrCpuDevCeil(Application app) {
        double sum = 0;
        for (int i = 0; i < timeSlotNB; i++) {
            sum += avCPU[i] - app.reqCPU[i];
        }
        return type.getIncrCpuDevCeil(type.getCpuDevCeil(sum / timeSlotNB));
    }

    public double getSumAvCPU() {
        double sum = 0;
        for (int i = 0; i < timeSlotNB; i++) {
            sum += avCPU[i];
        }
        return sum;
    }

    public void updateCpuDev() {
        assert criterMode == CriterMode.CPU_DEV : "Machine : unexpected CriterMode !";

        double sum = 0;
        for (int i = 0; i < timeSlotNB; i++) {
            sum += avCPU[i];
        }
        double mean = sum / timeSlotNB;

        sum = 0;
        for (int i = 0; i < timeSlotNB; i++) {
            sum += Math.abs(avCPU[i] - mean);
        }
        criterion = sum / timeSlotNB / type.capCPU;
    }

    public void setCpuDev(double cpuDev) {
        assert criterMode == CriterMode.CPU_DEV : "Machine : unexpected CriterMode !";
        criterion = cpuDev;
    }

    public double getCpuDev() {
        if (criterMode == CriterMode.CPU_DEV) {
            return criterion;
        }

        double sum = 0;
        for (int i = 0; i < timeSlotNB; i++) {
            sum += avCPU[i];
        }
        double mean = sum / timeSlotNB;

        sum = 0;
        for (int i = 0; i < timeSlotNB; i++) {
            sum += Math.abs(avCPU[i] - mean);
        }
        return sum / timeSlotNB / type.capCPU;
    }

    public void updateDis2MaxCpuCon() {
        assert criterMode == CriterMode.DIS2MAX_CPU_CON : "Machine : unexpected CriterMode !";

        criterion = Double.MAX_VALUE;
        for (double cpu : avCPU) {
            if (criterion > cpu) {
                criterion = cpu;
            }
        }
    }

    public double getDis2MaxCpuCon() {
        if (criterMode == CriterMode.DIS2MAX_CPU_CON) {
            return criterion;
        }

        if (isEmpty()) {
            return Double.MIN_VALUE;
        }

        double min = Double.MAX_VALUE;
        for (double cpu : avCPU) {
            if (min > cpu) {
                min = cpu;
            }
        }

        return min - type.getMinAvCPU();
    }

    public int getAvDISK() {
        return avDISK;
    }

    public void swap(Machine m) {
        int i = id;
        id = m.id;
        m.id = i;

        id2machine.put(id, this);
        id2machine.put(m.id, m);
    }

    public void reset() {
        for (int i = 0; i < timeSlotNB; i++) {
            avCPU[i] = type.capCPU;
        }
        for (int i = 0; i < timeSlotNB; i++) {
            avMEM[i] = type.capMEM;
        }

        avDISK = type.capDISK;
        avP = type.capP;
        avM = type.capM;
        avPM = type.capPM;

        hostedApps.clear();

        criterion = 0;
    }

    public static void resetAll() {
        for (Machine m : considerMachines) {
            m.reset();
        }
    }

    public boolean isEmpty() {
        return hostedApps.isEmpty();
    }

    public Map<Application, Integer> getHostedApps() {
        return hostedApps;
    }

    public int getID() {
        return id;
    }

    public static int string2ID(String s) {
        return Integer.parseInt(s.substring(prefix.length()));
    }

    public static Machine getMachine(int id) {
        return id2machine.get(id);
    }

    public static Collection<Machine> getAllMachines() {
        return id2machine.values();
    }

    public static int getOccupiedMachineNB() {
        return occupiedMachineNB;
    }

    public static void setOccupiedMachineNB(int occupiedMachineNB) {
        Machine.occupiedMachineNB = occupiedMachineNB;
    }

    public static void decrOccupiedMachineNB() {
        occupiedMachineNB--;
    }

    public static void incrOccupiedMachineNB() {
        occupiedMachineNB++;
    }

    public void debug() {
        int disk = avDISK;
        int p = avP;
        int m = avM;
        int pm = avPM;

        double[] cpu = new double[timeSlotNB];
        double[] mem = new double[timeSlotNB];

        for (int i = 0; i < timeSlotNB; i++) {
            cpu[i] = avCPU[i];
            mem[i] = avMEM[i];
        }

        assert disk >= 0 : "negative DISK";
        assert p >= 0 : "negative P";
        assert m >= 0 : "negative M";
        assert pm >= 0 : "negative PM";

        for (int i = 0; i < timeSlotNB; i++) {
            assert cpu[i] > -Utils.EPSILON : "negative CPU";
            assert mem[i] > -Utils.EPSILON : "negative MEM";
        }

        for (Entry<Application, Integer> entry : hostedApps.entrySet()) {
            Application app = entry.getKey();
            int nb = entry.getValue();

            disk += app.reqDISK * nb;
            p += app.reqP * nb;
            m += app.reqM * nb;
            pm += app.reqPM * nb;

            for (int i = 0; i < timeSlotNB; i++) {
                cpu[i] += app.reqCPU[i] * nb;
                mem[i] += app.reqMEM[i] * nb;
            }
        }

        assert disk == type.capDISK : "DISK cap diff : " + disk + ", " + type.capDISK;
        assert p == type.capP : "P cap diff : " + p + ", " + type.capP;
        assert m == type.capM : "M cap diff : " + m + ", " + type.capM;
        assert pm == type.capPM : "PM cap diff : " + pm + ", " + type.capPM;

        for (int i = 0; i < timeSlotNB; i++) {
            assert Utils.doubleEqual(cpu[i], type.capCPU) : "CPU cap diff : " + cpu[i] + ", " + type.capCPU;
            assert Utils.doubleEqual(mem[i], type.capMEM) : "MEM cap diff : " + mem[i] + ", " + type.capMEM;
        }

        int count;
        for (Entry<Application, Integer> entry : hostedApps.entrySet()) {
            count = 0;
            for (Instance inst : entry.getKey().inst_list) {
                if (inst.getTarHost() == this) {
                    count++;
                }
            }

            assert count == entry.getValue() : "inconsistent hosting state !";
        }
    }

    public void print() {
        Printer.info(id + " :");

        double max = 0;
        double min = Double.MAX_VALUE;
        double mean = 0;
        for (double cpu : avCPU) {
            if (max < cpu) {
                max = cpu;
            }
            if (min > cpu) {
                min = cpu;
            }
            mean += cpu;
        }

        max = 1 - max / type.capCPU;
        min = 1 - min / type.capCPU;
        mean = 1 - mean / timeSlotNB / type.capCPU;

        Printer.print("  cpu      " + min + ", " + mean + ", " + max);

        Printer.print("  disk " + (1 - 1.0 * avDISK / type.capDISK));

        max = 0;
        min = Double.MAX_VALUE;
        mean = 0;
        for (double mem : avMEM) {
            if (max < mem) {
                max = mem;
            }
            if (min > mem) {
                min = mem;
            }
            mean += mem;
        }

        min = 1 - min / type.capMEM;
        mean = 1 - mean / timeSlotNB / type.capMEM;
        max = 1 - max / type.capMEM;

        Printer.print("  mem " + min + ", " + mean + ", " + max);

        int sum = 0;
        for (int i : hostedApps.values()) {
            sum += i;
        }

        Printer.print("  hosting " + sum);
    }

    public static void analyse() {
        // Collections.sort(considerMachines, new Comparator<Machine>() {
        // @Override
        // public int compare(Machine m1, Machine m2) {
        // double s1 = m1.getScore();
        // double s2 = m2.getScore();
        //
        // return s1 < s2 ? 1 : s1 == s2 ? 0 : -1;
        // }
        // });

        // Collections.sort(considerMachines, new Comparator<Machine>() {
        // @Override
        // public int compare(Machine m1, Machine m2) {
        // if (m1.isEmpty() && !m2.isEmpty()) {
        // return 1;
        // } else if (!m1.isEmpty() && m2.isEmpty()) {
        // return -1;
        // }
        //
        // double min1 = Double.MAX_VALUE;
        // for (double cpu : m1.avCPU) {
        // if (min1 > cpu) {
        // min1 = cpu;
        // }
        // }
        // min1 /= m1.type.capCPU;
        //
        // double min2 = Double.MAX_VALUE;
        // for (double cpu : m2.avCPU) {
        // if (min2 > cpu) {
        // min2 = cpu;
        // }
        // }
        // min2 /= m1.type.capCPU;
        //
        // return min1 < min2 ? -1 : min1 == min2 ? 0 : 1;
        // }
        // });

        Collections.sort(considerMachines, new Comparator<Machine>() {
            @Override
            public int compare(Machine m1, Machine m2) {
                if (m1.isEmpty() && !m2.isEmpty()) {
                    return 1;
                } else if (!m1.isEmpty() && m2.isEmpty()) {
                    return -1;
                }

                return m1.avDISK > m2.avDISK ? -1 : m1.avDISK == m2.avDISK ? 0 : 1;
            }
        });

        // Collections.sort(considerMachines, new Comparator<Machine>() {
        // @Override
        // public int compare(Machine m1, Machine m2) {
        // if (m1.isEmpty() && !m2.isEmpty()) {
        // return 1;
        // } else if (!m1.isEmpty() && m2.isEmpty()) {
        // return -1;
        // }
        //
        // int hostedInstNB1 = 0;
        // for (int i : m1.hostedApps.values()) {
        // hostedInstNB1 += i;
        // }
        //
        // int hostedInstNB2 = 0;
        // for (int i : m2.hostedApps.values()) {
        // hostedInstNB2 += i;
        // }
        //
        // return hostedInstNB1 < hostedInstNB2 ? -1 : hostedInstNB1 ==
        // hostedInstNB2 ? 0 : 1;
        // }
        // });

        for (int i = 0; i < 100; i++) {
            considerMachines.get(i).print();
        }
    }

    public static int getHostedInstCount() {
        int sum = 0;
        for (Machine m : Machine.getAllMachines()) {
            for (int i : m.hostedApps.values()) {
                sum += i;
            }
        }

        return sum;
    }

    @Override
    public int hashCode() {
        return index;
    }

    @Override
    public boolean equals(Object obj) {
        assert obj.getClass() == getClass() : "Application : unexpected input !";

        return obj.hashCode() == hashCode();
    }
}

