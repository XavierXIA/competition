/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package model.infra;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import common.Utils;

public final class MachineType {
    public static final List<MachineType> type_list = new ArrayList<>(2);
    public static final Map<String, MachineType> MachineType_map = new HashMap<>(2);
    static final boolean[] isConsiderNegDis = new boolean[Machine.timeSlotNB];

    public final double capCPU;
    public final double capMEM;
    public final int capDISK;
    public final int capP;
    public final int capM;
    public final int capPM;

    private int expectedNB;
    private double expectedScore;
    double[] expectedAvCpu = null;

    // how to change dynamically, two directions ?
    private double meanCpuConCeil = 0.6; // a : 0.485
    private double CpuConCeil = 0.75; // [0.4, 0.5), a : 0.75
    private double cpuDevCeilCoeff = 2.2; // (1.648721, xxx)
    private double initCpuDevCeil = 0.5; // (, 0.5)
    private double incrCpuDevRatio = 0.3; // ? a : 0.3

    private double minMeanAvCPU;
    private double minAvCPU;

    public MachineType(String[] s) {
        capCPU = Double.parseDouble(s[0]);
        capMEM = Double.parseDouble(s[1]);
        capDISK = Integer.parseInt(s[2]);
        capP = Integer.parseInt(s[3]);
        capM = Integer.parseInt(s[4]);
        capPM = Integer.parseInt(s[5]);

        minMeanAvCPU = (1 - meanCpuConCeil) * capCPU;
        minAvCPU = (1 - CpuConCeil) * capCPU;

        expectedScore = 0;
    }

    public int getExpectedNB() {
        return expectedNB;
    }

    public void setExpectedNB(int expectedNB) {
        this.expectedNB = expectedNB;
    }

    public void setCpuDevCeilCoeff(double cpuDevCeilCoeff) {
        this.cpuDevCeilCoeff = cpuDevCeilCoeff;
    }

    public void setInitCpuDevCeil(double initCpuDevCeil) {
        this.initCpuDevCeil = initCpuDevCeil;
    }

    public void setIncrCpuDevRatio(double incrCpuDevRatio) {
        this.incrCpuDevRatio = incrCpuDevRatio;
    }

    public void setMeanCpuConCeil(double meanCpuConCeil) {
        this.meanCpuConCeil = meanCpuConCeil;
    }

    public void setCpuConCeil(double cpuConCeil) {
        CpuConCeil = cpuConCeil;
    }

    public double getMinMeanAvCPU() {
        return minMeanAvCPU;
    }

    public double getMinAvCPU() {
        return minAvCPU;
    }

    public double getFevorableMeanAvCpuRate() {
        return 1 - meanCpuConCeil;
    }

    public double getCpuDevCeil(double meanAvCPU) {
        double rate = 1 - meanAvCPU / capCPU;
        return (cpuDevCeilCoeff - Math.exp(rate)) * initCpuDevCeil / (cpuDevCeilCoeff - 1);
    }

    public double getIncrCpuDevCeil(double fitThred) {
        return fitThred * incrCpuDevRatio;
    }

    public double getExpectedScore() {
        return expectedScore;
    }

    public void setExpectedScore(double expectedScore) {
        this.expectedScore = expectedScore;
    }

    public void setExpectedAvCpu(double[] expectedAvCpuRate) {
        expectedAvCpu = expectedAvCpuRate;

        expectedScore = 0;
        for (int i = 0; i < expectedAvCpu.length; i++) {
            if (expectedAvCpu[i] < 0.5) {
                isConsiderNegDis[i] = true;
            }

            expectedScore += 1 + 10 * (Math.exp(Math.max(0, 0.5 - expectedAvCpu[i])) - 1);
            expectedAvCpu[i] = expectedAvCpu[i] * capCPU;
        }
        expectedScore /= expectedAvCpu.length;
    }

    public String randomParas() {
        meanCpuConCeil = 0.4 + Utils.nextDouble() / 5;
        CpuConCeil = 0.5 + Utils.nextDouble() / 3;
        cpuDevCeilCoeff = 1.65 + Utils.nextDouble() / 3;
        initCpuDevCeil = 0.1 + Utils.nextDouble() / 3;
        incrCpuDevRatio = Utils.nextDouble();

        for (MachineType type : type_list) {
            type.minMeanAvCPU = (1 - meanCpuConCeil) * type.capCPU;
            type.minAvCPU = (1 - CpuConCeil) * type.capCPU;
        }

        return meanCpuConCeil + ", " + CpuConCeil + ", " + cpuDevCeilCoeff + ", " + initCpuDevCeil + ", "
                + incrCpuDevRatio;
    }

    public static MachineType getMachineType(String config) {
        if (MachineType_map.containsKey(config)) {
            return MachineType_map.get(config);
        }

        MachineType type = new MachineType(config.split(","));
        MachineType_map.put(config, type);

        type_list.add(type);
        Collections.sort(type_list, new Comparator<MachineType>() {
            @Override
            //assumption : t1.capCPU != t2.capCPU
            public int compare(MachineType t1, MachineType t2) {
                return t1.capCPU > t2.capCPU ? -1 : 1;
            }
        });

        return type;
    }

    // TODO : not generic
    @Override
    public int hashCode() {
        return capDISK;
    }

    @Override
    public boolean equals(Object obj) {
        assert obj.getClass() == getClass() : "Application : unexpected input !";

        return obj.hashCode() == hashCode();
    }
}

