/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package model.app;

import improvement.global.MachineSealer;
import model.infra.Machine;

public final class Instance {
    public static final String prefix = "inst_";

    private int id;
    private int initHostID;

    private Machine currHost;
    private Machine tarHost;

    public Instance(String[] s) {
        id = Integer.parseInt(s[0].substring(prefix.length()));

        if (s.length < 3) {
            initHostID = -1;
        } else {
            initHostID = Machine.string2ID(s[2]);
        }

        currHost = null;
        tarHost = null;

        Application.getApp(s[1]).addInst(this);
    }

    public boolean hasInitHost() {
        return initHostID >= 0;
    }

    public int getInitHostID() {
        return initHostID;
    }

    public boolean isPlaced() {
        return tarHost != null;
    }

    public void setTarHost(Machine m) {
        tarHost = m;
    }

    public Machine getTarHost() {
        return tarHost;
    }

    public void setCurrHost(Machine m) {
        currHost = m;
    }

    public Machine getCurrHost() {
        return currHost;
    }

    public void swap(Instance inst) {
        int i = id;
        id = inst.id;
        inst.id = i;

        i = initHostID;
        initHostID = inst.initHostID;
        inst.initHostID = i;
    }

    public int getID() {
        return id;
    }

    public static void initCurrHosts() {
        for (Application app : Application.app_list) {
            for (Instance inst : app.inst_list) {
                if (inst.hasInitHost()) {
                    inst.currHost = Machine.getMachine(inst.initHostID);
                }
              //TODO
//                else {
//                    inst.currHost = null;
//                }
            }
        }
    }

    public static void resetAll() {
        for (Application app : Application.app_list) {
            for (Instance inst : app.inst_list) {
                if (!MachineSealer.isSealed(inst.tarHost)) {
                    inst.currHost = null;
                    inst.tarHost = null;
                }
            }
        }
    }
}
