/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package model.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.infra.Machine;

import common.Printer;

import control.Main;

public final class Application {
    public static final String prefix = "app_";
    public static final Map<Integer, Application> id2app = new HashMap<>(8192);
    public static final List<Application> app_list = new ArrayList<>(9338);
    private static double cpuStableThreshold = 0.1;

    public final int id;
    public final int index;
    public final List<Instance> inst_list;

    public final double[] reqCPU;
    public final double[] reqMEM;
    public final int reqDISK;
    public final int reqP;
    public final int reqM;
    public final int reqPM;

    public final double maxCPU;
    public final double meanCPU;
    public final double maxMEM;
    public final boolean isStable;

    public static enum AppOrderMode {
        MAX_MIN_DIS, VAR_CPU, RANDOM
    };

    private static AppOrderMode orderMode = AppOrderMode.RANDOM;
    private double criterion;

    private static final Comparator<Application> comparator = new Comparator<Application>() {
        @Override
        public int compare(Application app1, Application app2) {
            if (app1.isStable && !app2.isStable) {
                return 1;
            } else if (!app1.isStable && app2.isStable) {
                return -1;
            } else if (app1.isStable && app2.isStable) {
                return app1.reqCPU[0] < app2.reqCPU[0] ? 1 : app1.reqCPU[0] == app2.reqCPU[0] ? 0 : -1;
            }

            return app1.criterion < app2.criterion ? 1 : app1.criterion == app2.criterion ? 0 : -1;
        }
    };

    public Application(String[] s, int index) {
        id = Integer.parseInt(s[0].substring(prefix.length()));
        this.index = index;
        id2app.put(id, this);
        app_list.add(this);
        inst_list = new ArrayList<>();

        double max = -1;
        double min = Double.MAX_VALUE;
        double sum = 0;
        String[] cpu = s[1].split("\\|");
        reqCPU = new double[cpu.length];
        for (int i = 0; i < cpu.length; i++) {
            reqCPU[i] = Double.parseDouble(cpu[i]);

            if (max < reqCPU[i]) {
                max = reqCPU[i];
            }
            if (min > reqCPU[i]) {
                min = reqCPU[i];
            }
            sum += reqCPU[i];
        }

        maxCPU = max;
        meanCPU = sum / cpu.length;

        if (max - min < cpuStableThreshold) {
            isStable = true;
        } else {
            isStable = false;
        }

        String[] mem = s[2].split("\\|");
        reqMEM = new double[mem.length];
        max = -1;
        for (int i = 0; i < mem.length; i++) {
            reqMEM[i] = Double.parseDouble(mem[i]);

            if (max < reqMEM[i]) {
                max = reqMEM[i];
            }
        }
        maxMEM = max;

        reqDISK = (int) Double.parseDouble(s[3]);
        reqP = Integer.parseInt(s[4]);
        reqM = Integer.parseInt(s[5]);
        reqPM = Integer.parseInt(s[6]);
    }

    public static Application getApp(String id) {
        return id2app.get(Integer.parseInt(id.substring(prefix.length())));
    }

    public static Application getApp(int id) {
        return id2app.get(id);
    }

    public static void clearIdAppMap() {
        id2app.clear();
    }

    public void addInst(Instance inst) {
        inst_list.add(inst);
    }

    public Instance getInst(Machine host) {
        for (Instance inst : inst_list) {
            if (inst.getTarHost() == host) {
                return inst;
            }
        }

        if (Main.debug) {
            Printer.err("Application : no instance found ! ");
        }

        return null;
    }

    // TODO
    public int getRemainedInstCount() {
        int count = 0;
        for (Instance inst : inst_list) {
            if (!inst.isPlaced()) {
                count++;
            }
        }

        return count;
    }

    public static void updateAppOrder(AppOrderMode m) {
        if (orderMode == m) {
            return;
        }

        orderMode = m;
        if (m == AppOrderMode.RANDOM) {
            Collections.shuffle(app_list);
            return;
        }

        for (Application app : app_list) {
            app.updateCriterion();
        }

        Collections.sort(app_list, comparator);
    }

    private void updateCriterion() {
        // TODO
        if (isStable) {
            return;
        }

        switch (orderMode) {
            case MAX_MIN_DIS:
                double min = Double.MAX_VALUE;
                for (int i = 0; i < reqCPU.length; i++) {
                    if (min > reqCPU[i]) {
                        min = reqCPU[i];
                    }
                }
                criterion = maxCPU - min;
                break;
            case VAR_CPU:
                criterion = 0;
                for (int i = 0; i < reqCPU.length; i++) {
                    criterion += Math.abs(reqCPU[i] - meanCPU);
                }
                break;
            default:
                Printer.err("Application : unexpected ordering mode!");
                break;
        }
    }

    public void print() {
        Printer.info(id + " :");

        String cpuInfo = "";
        for (double cpu : reqCPU) {
            cpuInfo += cpu + " ";
        }
        Printer.pass("  cpu " + cpuInfo);

        System.out.println("  disk " + reqDISK);

        String memInfo = "";
        for (double mem : reqMEM) {
            memInfo += mem + " ";
        }
        System.out.println("  mem " + memInfo);
    }

    public static void setCpuStableThreshold(double cpuStableThreshold) {
        Application.cpuStableThreshold = cpuStableThreshold;
    }

    public static int getInstCount() {
        int count = 0;
        for (Application app : app_list) {
            count += app.inst_list.size();
        }

        return count;
    }

    public static int getHostedInstCount() {
        int count = 0;
        for (Application app : app_list) {
            for (Instance inst : app.inst_list) {
                if (inst.isPlaced()) {
                    count++;
                }
            }
        }

        return count;
    }

    @Override
    public int hashCode() {
        return index;
    }

    @Override
    public boolean equals(Object obj) {
        assert obj.getClass() == getClass() : "Application : unexpected input !";

        return obj.hashCode() == hashCode();
    }
}
