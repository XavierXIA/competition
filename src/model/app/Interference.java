/*
* Copyright @ Ye XIA <qazxiaye@126.com>
* 
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package model.app;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class Interference {
    public static final Map<Application, InterfWrapper> app_interf_map = new HashMap<>();

    private final Application srcApp;
    private final Application tarApp;
    private final int maxNB;

    public Interference(String[] s) {
        srcApp = Application.getApp(s[0]);
        tarApp = Application.getApp(s[1]);

        if (srcApp == tarApp) {
            maxNB = Integer.parseInt(s[2]) + 1;
        } else {
            maxNB = Integer.parseInt(s[2]);
        }

        AddInterf(srcApp, true);
        AddInterf(tarApp, false);
    }

    private void AddInterf(Application app, boolean isSrc) {
        InterfWrapper wrapper = app_interf_map.get(app);

        if (wrapper == null) {
            wrapper = new InterfWrapper();
            app_interf_map.put(app, wrapper);
        }

        if (isSrc) {
            wrapper.addSrcInterf(this);
        } else {
            wrapper.addTarInterf(this);
        }
    }

    // assumption : init status valid
    public static boolean isInterfered(Map<Application, Integer> hostedApps, Application app2place) {
        InterfWrapper wrapper = app_interf_map.get(app2place);
        if (wrapper == null) {
            return false;
        }

        int hostedNB = 0;
        Integer i = hostedApps.get(app2place);
        if (i != null) {
            hostedNB = i;
        } else {
            for (Interference interf : wrapper.srcAppInterfs) {
                Integer nb = hostedApps.get(interf.tarApp);

                if (nb != null && nb > interf.maxNB) {
                    return true;
                }
            }
        }

        for (Interference interf : wrapper.tarAppInterfs) {
            if (hostedApps.containsKey(interf.srcApp)) {
                if (hostedNB + 1 > interf.maxNB) {
                    return true;
                }
            }
        }

        return false;
    }

    // init status not necessarily valid
    public static boolean isHardInterfered(Map<Application, Integer> hostedApps, Application app2place) {
        InterfWrapper wrapper = app_interf_map.get(app2place);
        if (wrapper == null) {
            return false;
        }

        int hostedNB = 0;
        Integer i = hostedApps.get(app2place);
        if (i != null) {
            hostedNB = i;
        }

        for (Interference interf : wrapper.srcAppInterfs) {
            Integer nb = hostedApps.get(interf.tarApp);

            if (nb != null && nb > interf.maxNB) {
                return true;
            }
        }

        for (Interference interf : wrapper.tarAppInterfs) {
            if (hostedApps.containsKey(interf.srcApp)) {
                if (hostedNB + 1 > interf.maxNB) {
                    return true;
                }
            }
        }

        return false;
    }

    public static int getInterfNB(Application app) {
        InterfWrapper w = app_interf_map.get(app);
        if (w == null) {
            return 0;
        }

        return w.srcAppInterfs.size() + w.tarAppInterfs.size();
    }
}

class InterfWrapper {
    public final Set<Interference> srcAppInterfs;
    public final Set<Interference> tarAppInterfs;

    public InterfWrapper() {
        tarAppInterfs = new HashSet<>();
        srcAppInterfs = new HashSet<>();
    }

    public void addSrcInterf(Interference interf) {
        srcAppInterfs.add(interf);
    }

    public void addTarInterf(Interference interf) {
        tarAppInterfs.add(interf);
    }
}

