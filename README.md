This project makes decisions for scheduling applications in a cloud center.
The optimization objective is to minimize energy consumption while taking applications' performance and resource requirements into account.

Please refer to [Scheduling Algorithm Competition (Preliminary)](https://tianchi.aliyun.com/competition/rankingList.htm?spm=5176.11165320.0.0.12b351ecLql9Sg&raceId=231663&season=0) for input data and more details.

Competition Rank : 2 / 2116


Compile:

`mvn package`


license: Apache License 2.0
